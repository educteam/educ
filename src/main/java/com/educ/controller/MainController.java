package com.educ.controller;

import com.educ.service.JDBCUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;
import java.util.logging.Logger;

@org.springframework.stereotype.Controller

public class MainController {

    private static Logger log = Logger.getLogger(MainController.class.getName());

    @RequestMapping("/adminpanel")
    public String run(){
        //SecurityContextHolderAwareRequestWrapper.isUserInRole(String role)
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = null;
        if (principal instanceof UserDetails) {
            userDetails = (UserDetails) principal;
        }
        return "admin";
    }

    @RequestMapping("/")
    public String runs(){
        return "index";
    }

    @RequestMapping("/backdoorlogin")
    public ModelAndView login(@RequestParam Optional<String> error){
        //CsrfToken token = new HttpSessionCsrfTokenRepository().loadToken(request);
        //System.out.println(error.toString());
        //model.put("_csrf",token);
        return new ModelAndView("login", "error", error);
    }

    @RequestMapping("/api/private/getObjectId")
    @ResponseBody
    public long getObjectID() throws SQLException {
        return JDBCUtils.getInstance().generateId();
    }
}
