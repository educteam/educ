package com.educ.controller;

import com.educ.model.Answer;
import com.educ.model.generationForm.UISecureMap;
import com.educ.service.*;
import com.educ.model.filter.FilteredDataRequest;
import com.educ.model.QuestionnaireAnswer;
import com.educ.model.generationForm.FormField;
import com.educ.model.generationForm.FormOption;
import com.educ.model.generationForm.UIForm;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RestController
public class QuestionnairesController {

    @RequestMapping(value = "/api/public/submitQuestionnaire", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void submitQuestionnaire(@RequestBody UIForm uiFormWithAnswers,
                                    HttpServletResponse response) throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        long questionnaireId = jdbcUtils.generateId();
        List<Answer> answers = new ArrayList<>();
        for (FormField formField : uiFormWithAnswers.getForm_fields()){
            if (formField.getField_type() == null) continue;
            Answer answer;
            String value = formField.getField_value();
            Boolean isRequired = formField.getField_required();
            long fieldId = formField.getId();
            Boolean wasSelectedAtLeastOne = false;
            if (formField.getField_options() != null && formField.getField_options().size() != 0){
                for(FormOption formOption : formField.getField_options()){
                    String optionValue = formOption.getTitle();
                    if(formOption.getOption_selected()){
                        wasSelectedAtLeastOne = true;
                        answer = new Answer(questionnaireId, fieldId, optionValue, isRequired);
                        answers.add(answer);
                    }
                }
                if (!wasSelectedAtLeastOne){
                    answer = new Answer(questionnaireId, fieldId, "", isRequired);
                    answers.add(answer);
                }
            }
            else {
                answer = new Answer(questionnaireId, fieldId, value, isRequired);
                answers.add(answer);
            }
        }
        if (uiFormWithAnswers.getObjectTypeName() != null) {
            CurrentSecurityMap securMap = new CurrentSecurityMap();
            switch (uiFormWithAnswers.getObjectTypeName()) {
                case (Constants.CURATOR_FORM_NAME):
                    if(securMap.hasCuratorGrants()) {
                        jdbcUtils.insertCuratorQuestionnaire(answers, uiFormWithAnswers.getDependentQueId());
                    }
                    break;
                case (Constants.HR_FORM_NAME):
                    if(securMap.hasHRGrants()) {
                        jdbcUtils.insertHRQuestionnaire(answers, uiFormWithAnswers.getDependentQueId());
                    }
                    break;
                default:
                    jdbcUtils.insertStudentQuestionnaire(answers, uiFormWithAnswers.getId());
                    break;
            }
        }
        else{
            jdbcUtils.insertStudentQuestionnaire(answers, uiFormWithAnswers.getId());
        }
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = "/api/private/exportCompositeTable", method = RequestMethod.GET)
    public void exportCompositeTable(@RequestParam(value="formId") long formId, HttpServletResponse response) throws SQLException, IOException, IllegalAccessException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if (securityMap.hasHRGrants()) {
            ApachePoiUtils apachePoiUtils = new ApachePoiUtils();
            HSSFWorkbook workbook = apachePoiUtils.prepareXLSAsWorkBook(formId);
            if (workbook == null) {
                throw new FileNotFoundException();
            }
            writeWorkBookToResponse(response, workbook);
        } else {
            throw new IllegalAccessException();
        }
    }

    @RequestMapping(value = "/api/private/exportFilteredTable", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void exportFilteredTable(@RequestBody FilteredDataRequest filteredDataRequest,
                                            HttpServletResponse response) throws SQLException, IOException, IllegalAccessException {
        Map<String, Map<Long, String>> rolesFilterFields = new HashMap<>();
        rolesFilterFields.put(Constants.ROLE_STUDENT, filteredDataRequest.getStudentFiltersAsHashMap());
        rolesFilterFields.put(Constants.ROLE_CURATOR, filteredDataRequest.getCuratorFiltersAsHashMap());
        rolesFilterFields.put(Constants.ROLE_HR, filteredDataRequest.getHRFiltersAsHashMap());

        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if (securityMap.hasHRGrants()) {
            ApachePoiUtils apachePoiUtils = new ApachePoiUtils();
            HSSFWorkbook workbook = apachePoiUtils.prepareXLSAsWorkBook(filteredDataRequest.getFormId(),
                    rolesFilterFields, filteredDataRequest.getShownFields());
            if (workbook == null) {
                throw new FileNotFoundException();
            }
            writeWorkBookToResponse(response, workbook);
        } else {
            throw new IllegalAccessException();
        }
    }

    @RequestMapping(value = "/api/private/exportQueAsDocx", method = RequestMethod.GET)
    public void exportQueAsDocx(@RequestParam(value="queId") long queId,
                                HttpServletResponse response) throws SQLException, IOException, IllegalAccessException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if (!securityMap.hasHRGrants()) {
            throw new IllegalAccessException();
        }
        ApachePoiUtils apachePoiUtils = new ApachePoiUtils();

        XWPFDocument doc = new XWPFDocument();
        String fileName = apachePoiUtils.exportDocxByStudQueId(queId, doc);
        //String fileName = "export.docx";
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        doc.write(outByteStream);
        byte[] outArray = outByteStream.toByteArray();
        response.setContentType("application/msword");
        response.setContentLength(outArray.length);
        response.setHeader("Expires:", "0"); // eliminates browser caching
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        OutputStream outStream = response.getOutputStream();
        outStream.write(outArray);
        outStream.flush();
    }

    private void writeWorkBookToResponse(HttpServletResponse response, HSSFWorkbook workbook) throws IOException {
        if (workbook == null) {
            throw new FileNotFoundException();
        }
        String fileName = "Questionnaires export data.xls";
        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        workbook.write(outByteStream);
        byte[] outArray = outByteStream.toByteArray();
        response.setContentType("application/ms-excel");
        response.setContentLength(outArray.length);
        response.setHeader("Expires:", "0"); // eliminates browser caching
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        OutputStream outStream = response.getOutputStream();
        outStream.write(outArray);
        outStream.flush();
    }

    @RequestMapping(value = "/api/private/getFilteredQuestionnaire", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public String getFilteredQuestionnaire(@RequestBody FilteredDataRequest filteredDataRequest) throws SQLException {

        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        Map<String, Map<Long, String>> rolesFilterFields = new HashMap<>();
        rolesFilterFields.put(Constants.ROLE_STUDENT, filteredDataRequest.getStudentFiltersAsHashMap());
        rolesFilterFields.put(Constants.ROLE_CURATOR, filteredDataRequest.getCuratorFiltersAsHashMap());
        rolesFilterFields.put(Constants.ROLE_HR, filteredDataRequest.getHRFiltersAsHashMap());
        String compositeQuestionnaireTable = jdbcUtils.getCompositeQuestionnaireTableByFormId(
                filteredDataRequest.getFormId(), rolesFilterFields).toString();
        return compositeQuestionnaireTable;
    }

    @RequestMapping(value = "/api/private/getQuestionnaires",method = RequestMethod.GET)
    public String getJSONForm(@RequestParam(value="id", defaultValue="2003") String id) throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        String compositeQuestionnaireTable = jdbcUtils.getCompositeQuestionnaireTableByFormId(Long.parseLong(id)).toString();
        return compositeQuestionnaireTable;
    }

    @RequestMapping(value = "/api/private/getAnswers",method = RequestMethod.GET)
    public List<QuestionnaireAnswer> getAnswers(@RequestParam(value="id") String id) throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        List<QuestionnaireAnswer> answer = jdbcUtils.getStudentQueById(Long.parseLong(id));
        return answer;
    }

    @RequestMapping(value = "/api/private/deleteQue", method = RequestMethod.GET)
    public void deleteQue(@RequestParam(value="queId") long formId) throws SQLException, IOException, IllegalAccessException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if (securityMap.hasAdminGrants()) {
            JDBCUtils jdbcUtils = JDBCUtils.getInstance();
            jdbcUtils.deleteQueById(formId);
        }else{
            throw new IllegalAccessException();
        }
    }


}
