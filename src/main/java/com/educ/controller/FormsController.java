package com.educ.controller;

import com.educ.model.Element;
import com.educ.model.generationForm.*;
import com.educ.service.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.educ.service.Constants.*;

@org.springframework.stereotype.Controller
@RestController
public class FormsController {
    @RequestMapping(value = "/api/private/createForm", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void setForm(@RequestBody CompositeUIForm generateForm) throws SQLException {
        List<Element> form = new ArrayList<>();
        propagateElementsOnOneForm(generateForm.getStudentForm(), form, OT_STUDENT_FORM);
        propagateElementsOnOneForm(generateForm.getCuratorForm(), form, OT_CURATOR_FORM);
        propagateElementsOnOneForm(generateForm.getHrForm(), form, OT_HR_FORM);
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        jdbcUtils.insertForm(form);
    }

    @RequestMapping(value = "/api/private/updateForm", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void updateForm(@RequestBody CompositeUIForm generateForm) throws SQLException {
        List<Element> form = new ArrayList<>();
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        propagateElementsOnOneForm(generateForm.getStudentForm(), form, Constants.OT_STUDENT_FORM);
        jdbcUtils.updateStudentForm(form);
        form.clear();
        propagateElementsOnOneForm(generateForm.getCuratorForm(), form, OT_CURATOR_FORM);
        jdbcUtils.updateCuratorForm(form);
        form.clear();
        propagateElementsOnOneForm(generateForm.getHrForm(),form,Constants.OT_HR_FORM);
        jdbcUtils.updateHRForm(form);
        form.clear();
    }

    private void propagateElementsOnOneForm(UIForm form, List<Element> elements, Long objectType){
        long formId = form.getId();
        long parentId = formId;
        Long[] parents = new Long[]{};
        if(elements.size() > 0 && (objectType == OT_CURATOR_FORM || objectType == OT_HR_FORM)){
            parents = new Long[]{elements.get(0).getObjectId()};
            parentId = elements.get(0).getObjectId();
        }
        elements.add(new Element(formId, parentId, objectType, form.getTitle(),
                null, false, parents, form.getOrder(), false));
        for (FormField formFields : form.getForm_fields()) {
            long elementId = formFields.getId();
            String elementName = formFields.getTitle();
            String elementTypeStr = formFields.getField_type();
            String elementDescription = formFields.getDescription();
            Long elementOrder = formFields.getOrder();
            Boolean isObsolete = formFields.getField_obsolete();
            isObsolete = isObsolete != null ? isObsolete : false;
            boolean elementIsRequired = formFields.getField_required();
            parents = new Long[]{formId};

            elements.add(new Element(elementId, formId, (Long) OBJECT_TYPES.get(elementTypeStr),
                    elementName, elementDescription, elementIsRequired,  parents, elementOrder, isObsolete));

            if (formFields.getField_options() != null) {
                for (FormOption formOptions : formFields.getField_options()) {
                    long childElementId = formOptions.getId();
                    String childElementName = formOptions.getTitle();
                    parents = new Long[]{elementId,formId};
                    Long optionOrder = formOptions.getOrder();
                    isObsolete = formOptions.getOption_obsolete();
                    isObsolete = isObsolete != null ? isObsolete : false;
                    elements.add(new Element(childElementId, elementId, Constants.OT_ELEMENT,
                            childElementName, null, false, parents, optionOrder, isObsolete));
                }
            }
        }
    }

    @RequestMapping(value = "/api/public/getStudentFormForSurvey",method = RequestMethod.GET)
    public UIForm getStudentUIFormForSurvey(@RequestParam(value="id", defaultValue="1") String id){
        return getStudentUIForm(id, false);
    }

    @RequestMapping(value = "/api/public/getStudentFormForUpdate",method = RequestMethod.GET)
    public UIForm getStudentUIFormForUpdate(@RequestParam(value="id", defaultValue="1") String id){
        return getStudentUIForm(id, true);
    }

    public UIForm getStudentUIForm(String id, Boolean isForUpdate){
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        List<Element> form = null;
        try {
            form = jdbcUtils.getStudentFormById(Long.parseLong(id), isForUpdate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getUIStudentFormByFormElements(form);
    }

    @RequestMapping(value = "/api/private/getCuratorFormForUpdate",method = RequestMethod.GET)
    public UIForm getCuratorUIFormForUpdate(@RequestParam(value="id", defaultValue="1") String id){
        return getCuratorUIForm(id, true);
    }

    @RequestMapping(value = "/api/private/getCuratorForm",method = RequestMethod.GET)
    public UIForm getCuratorUIFormForCompositeTable(@RequestParam(value="id", defaultValue="1") String id){
        return getCuratorUIForm(id, false);
    }

    public UIForm getCuratorUIForm(String id, Boolean isForUpdate){
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        List<Element> form = null;
        try {
            form = jdbcUtils.getCuratorFormByStudentQuestionnaireId(Long.parseLong(id), isForUpdate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getUICuratorFormByFormElements(form, Long.parseLong(id));
    }

    @RequestMapping(value = "/api/private/getHRFormForUpdate",method = RequestMethod.GET)
    public UIForm getHRUIFormForUpdate(@RequestParam(value="id", defaultValue="1") String id){
        return getHRUIForm(id, true);
    }

    @RequestMapping(value = "/api/private/getHRForm",method = RequestMethod.GET)
    public UIForm getHRUIFormForCompositeTable(@RequestParam(value="id", defaultValue="1") String id){
        return getHRUIForm(id, false);
    }

    public UIForm getHRUIForm( String id, boolean isForUpdate){
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        List<Element> form = null;
        try {
            form = jdbcUtils.getHRFormByStudentQuestionnaireId(Long.parseLong(id), isForUpdate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getUIHRFormByFormElements(form, Long.parseLong(id));
    }

    @RequestMapping(value = "/api/private/getGrants",method = RequestMethod.GET)
    public UISecureMap getGrants() throws SQLException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        UISecureMap uiSecureMap = new UISecureMap(securityMap.hasCuratorGrants(), securityMap.hasHRGrants(), securityMap.hasAdminGrants() );
        return uiSecureMap;
    }

    @RequestMapping(value = "/api/private/getForms",method = RequestMethod.GET)
    public Map<Long, Map<String, String>> getForms() throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        return jdbcUtils.getAllForms();
    }

    /*For student forms*/
    private UIForm getUIStudentFormByFormElements(List<Element> form) {
        return getUIFormByFormElements(form, Constants.OT_STUDENT_FORM, null);
    }

    /*For curator forms*/
    private UIForm getUICuratorFormByFormElements(List<Element> form, long dependentQueIdIfExist) {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if(securityMap.hasCuratorGrants()) return getUIFormByFormElements(form, OT_CURATOR_FORM, dependentQueIdIfExist);
        return null;
    }

    /*For hr forms*/
    private UIForm getUIHRFormByFormElements(List<Element> form, long dependentQueIdIfExist) {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if(securityMap.hasHRGrants()) return getUIFormByFormElements(form, OT_HR_FORM, dependentQueIdIfExist);
        return null;
    }

    /*TODO: Put the method to the Form OT*/
    private UIForm getUIFormByFormElements(List<Element> form, long objectTypeId, Long dependentQueIdIfExist) {
        UIForm uiForm;
        if (dependentQueIdIfExist == null) {
            uiForm = new UIForm();
        }
        else {
            uiForm = new UIForm(dependentQueIdIfExist);
        }
        for(Element element: form){

            long OT = element.getOT();
            if(OT == objectTypeId){
                uiForm.setId(element.getObjectId());
                uiForm.setTitle(element.getName());
                List<FormField> formFields = new ArrayList<>();
                List<Element> fields = findChildren(element,form);
                for (Element field : fields) {
                    FormField formField = new FormField();
                    formField.setId(field.getObjectId());
                    formField.setDescription(field.getDescription());
                    formField.setTitle(field.getName());
                    formField.setField_required(field.isRequired());
                    formField.setField_value(field.getValue());
                    formField.setField_type((String) Constants.OBJECT_TYPES.getKey(field.getOT()));
                    formField.setField_obsolete(field.getObsolete());
                    List<FormOption> formOptions = new ArrayList<>();
                    List<Element> options = findChildren(field,form);
                    for (Element option : options) {
                        FormOption formOption = new FormOption();
                        formOption.setId(option.getObjectId());
                        formOption.setTitle(option.getName());
                        formOption.setOption_value(option.getName());
                        boolean selected = (field.getValue()!=null && field.getValue().equals(option.getName()));
                        formOption.setOption_selected(selected);
                        formOption.setOption_obsolete(option.getObsolete());
                        formOptions.add(formOption);
                    }
                    formOptions.sort(new FieldComparator());
                    formField.setField_options(formOptions);
                    formFields.add(formField);

                }
                uiForm.setForm_fields(formFields);
            }
        }
        return uiForm;
    }

    private List<Element> findChildren(Element e, List<Element> form){
        List<Element> list = new ArrayList<>();

        for(int i = 0; i< form.size(); i++) {
            if(form.get(i).getParentId() == e.getObjectId() && form.get(i).getObjectId() != e.getObjectId()){
                list.add(form.get(i));
            }
        }
        return list;
    }
}
