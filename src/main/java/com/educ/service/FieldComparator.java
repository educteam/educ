package com.educ.service;

import com.educ.model.generationForm.UIElement;

import java.util.Comparator;

public class FieldComparator<T extends UIElement> implements Comparator<T> {
    @Override
    public int compare(T o1, T o2) {
        if(o1.getId() < o2.getId()){
            return -1;
        }
        if(o1.getId() > o2.getId()){
            return 2;
        }
        return 0;
    }
}
