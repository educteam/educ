package com.educ.service;

/**
 * Created by GriGri on 17.01.2017.
 */
public class CurrentSecurityMap {
    private final Boolean hasAdminGrants;
    private final Boolean hasCuratorGrants;
    private final Boolean hasHRGrants;
    private final Boolean hasManagerGrants;

    public CurrentSecurityMap(){
        hasAdminGrants = Utils.hasSecurityRole(Constants.ROLE_ADMIN);
        hasCuratorGrants = Utils.hasSecurityRole(Constants.ROLE_CURATOR);
        hasHRGrants = Utils.hasSecurityRole(Constants.ROLE_HR);
        hasManagerGrants = Utils.hasSecurityRole(Constants.ROLE_MANAGER);
    }

    public Boolean hasAdminGrants() {
        return hasAdminGrants;
    }

    public Boolean hasCuratorGrants() {
        return (hasCuratorGrants || hasHRGrants);
    }

    public Boolean hasHRGrants() {
        return hasHRGrants;
    }

    public Boolean hasManagerGrants() {
        return hasManagerGrants;
    }
}
