package com.educ.service;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xwpf.usermodel.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.educ.service.Constants.END_MARK;
import static com.educ.service.Constants.START_MARK;

/**
 * Created by grpa0216 on 07.02.2017.
 */
public class ApachePoiUtils {
    private boolean wasFirstReplace = false;

    public HSSFWorkbook prepareXLSAsWorkBook(long formId) throws SQLException, IllegalAccessException {
        return prepareXLSAsWorkBook(formId, new HashMap<>(), new ArrayList<>());
    }

    public HSSFWorkbook prepareXLSAsWorkBook(long formId, Map<String, Map<Long, String>> rolesFilterFields,
                                             List<Long> shownFieldsFilter) throws SQLException, IllegalAccessException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if (!securityMap.hasHRGrants()) {
            throw new IllegalAccessException();
        }
        HSSFWorkbook workbook = new HSSFWorkbook();
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        JSONObject jsonObject = jdbcUtils.getCompositeQuestionnaireTableByFormId(formId, rolesFilterFields, shownFieldsFilter);
        JSONArray studentTitles = jsonObject.getJSONArray("titles");
        JSONArray curatorTitles = jsonObject.getJSONArray("curatorTitles");
        JSONArray hrTitles = jsonObject.getJSONArray("hrTitles");
        HSSFSheet sheet = workbook.createSheet("FirstSheet");
        HSSFRow rowhead = sheet.createRow((short) 0);

        rowhead.createCell(0).setCellValue("Questionnaire id");
        int positionInXml = 1;
        for (JSONArray titles : new JSONArray[]{studentTitles, curatorTitles, hrTitles}) {
            if (titles != null) {
                for (int i = 0; i < titles.length(); i++) {
                    String value = (String) ((JSONObject) titles.get(i)).get("value");
                    rowhead.createCell(positionInXml++).setCellValue(value);
                }
            }
        }

        JSONArray answers = jsonObject.getJSONArray("answers");
        if (answers == null) {
            return new HSSFWorkbook();
        }
        for (int j = 0; j < answers.length(); j++) {
            HSSFRow row = sheet.createRow((short) j + 1);
            long id = (Long) ((JSONObject) answers.get(j)).get("id");
            row.createCell(0).setCellValue(id);
            JSONArray studentAnswers = (JSONArray) ((JSONObject) answers.get(j)).get("studentAnswers");
            JSONArray curatorAnswers = (JSONArray) ((JSONObject) answers.get(j)).get("curatorAnswers");
            JSONArray hrAnswers = (JSONArray) ((JSONObject) answers.get(j)).get("hrAnswers");
            positionInXml = 1;
            for (JSONArray answersInQue : new JSONArray[]{studentAnswers, curatorAnswers, hrAnswers}) {
                if (answersInQue == null) {
                    continue;
                }
                for (int i = 0; i < answersInQue.length(); i++) {
                    String value = (String) ((JSONObject) answersInQue.get(i)).get("field_value");
                    row.createCell(positionInXml++).setCellValue(value);
                }

            }
        }
        return workbook;
    }

    public String exportDocxByStudQueId(long studQueId, XWPFDocument doc) throws SQLException, IllegalAccessException, IOException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if (!securityMap.hasHRGrants()) {
            throw new IllegalAccessException();
        }
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        Map<Long, String> answersMap = jdbcUtils.getAllQuesByStudQueId(studQueId);
        doc = new XWPFDocument();
        String filePath = "/templates/ExportDocxTemplate.docx";
        String fileName = "export.docx";
        InputStream fileInputStream = new ClassPathResource(filePath).getInputStream();
        try {
            doc = new XWPFDocument(fileInputStream);
            doc = this.fullTemplate(doc, answersMap);

            String fullName = answersMap.get(2007L);
            int lastIndex =  fullName.indexOf(" ");
            if (lastIndex == -1){
                return TranslitUtil.toTranslit(fullName) + ".docx";
            }

            String secondName = fullName.substring(0, lastIndex );
            String io =  fullName.replace(secondName, "").trim();
            lastIndex =  io.indexOf(" ");

            if (lastIndex == -1){
                return TranslitUtil.toTranslit(fullName) + ".docx";
            }
            fileName = secondName + " " + io.substring(0, lastIndex) + ".docx";
            //doc.write(new FileOutputStream("Que " + new File(answersMap.get(2007l) + ".docx")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return TranslitUtil.toTranslit(fileName);
    }

    private XWPFDocument fullTemplate(XWPFDocument doc, Map<Long, String> answersMap){
        // REPLACE ALL HEADERS
        for (XWPFHeader header : doc.getHeaderList()) {
            replaceAllBodyElements(header.getBodyElements(), answersMap);
        }
        // REPLACE BODY
        replaceAllBodyElements(doc.getBodyElements(), answersMap);
        return doc;
    }

    private void replaceAllBodyElements(List<IBodyElement> bodyElements, Map<Long, String> answersMap){
        for (IBodyElement bodyElement : bodyElements) {
            if (bodyElement.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0)
                replaceParagraph((XWPFParagraph) bodyElement, answersMap);
            if (bodyElement.getElementType().compareTo(BodyElementType.TABLE) == 0)
                replaceTable((XWPFTable) bodyElement, answersMap);
        }
    }

    private void replaceTable(XWPFTable table, Map<Long, String> answersMap) {
        for (XWPFTableRow row : table.getRows()) {
            for (XWPFTableCell cell : row.getTableCells()) {
                for (IBodyElement bodyElement : cell.getBodyElements()) {
                    if (bodyElement.getElementType().compareTo(BodyElementType.PARAGRAPH) == 0) {
                        replaceParagraph((XWPFParagraph) bodyElement, answersMap);
                    }
                    if (bodyElement.getElementType().compareTo(BodyElementType.TABLE) == 0) {
                        replaceTable((XWPFTable) bodyElement, answersMap);
                    }
                }
            }
        }
    }

    private void replaceParagraph(XWPFParagraph paragraph, Map<Long, String> answersMap) {
        StringBuilder paragraphText = new StringBuilder();
        Utils utils = new Utils();
        for (XWPFRun r : paragraph.getRuns()) {
            paragraphText.append(r.getText(r.getTextPosition()));
            /*There was a problem with template - hidden null String at the top of document*/
            if (!wasFirstReplace && paragraphText.toString().equals("null")){
                paragraphText.setLength(0);
                wasFirstReplace = true;
            }
        }
        String text = paragraphText.toString();
        while (text.contains(START_MARK) && text.contains(END_MARK)) {
            String numberMark = text.substring(text.indexOf(START_MARK) + START_MARK.length(), text.indexOf(END_MARK));
            if (utils.tryParseLong(numberMark)) {
                Long markFieldId = Long.parseLong(numberMark);
                String value = answersMap.get(markFieldId);
                if (value == null){
                    value = "";
                }
                text = text.replace(START_MARK + numberMark + END_MARK, value);
                //run.setText(text, 0);
                changeTextInParagraph(paragraph, text);
            }
        }
    }

    private void changeTextInParagraph(XWPFParagraph p, String newText) {
        List<XWPFRun> runs = p.getRuns();
        for(int i = runs.size() - 1; i > 0; i--) {
            p.removeRun(i);
        }
        XWPFRun run = runs.get(0);
        run.setText(newText, 0);
    }
}
