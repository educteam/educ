package com.educ.service;

import com.educ.model.Answer;
import com.educ.model.Element;

import java.sql.SQLException;
import java.util.*;

public class QueGeneration {

    public void generateAndInsertForm(int rowCount) throws SQLException {
        List<Element> form = new ArrayList<>();
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        long formId = jdbcUtils.generateId();
        form.add(new Element(formId, formId, Constants.OT_STUDENT_FORM, "UIForm " + generateText(),
                "UIForm Description " + generateText(), false, new Long[]{}, (long)0, false));
        Random rand = new Random();
        for (int i = 0; i < rowCount; i++) {
            int ot = rand.nextInt(6) + 100;
            long elementId = jdbcUtils.generateId();
            form.add(new Element(elementId, formId, (long)ot, generateQuestionTitle(),
                    generateQuestionDescription(), rand.nextBoolean(), new Long[]{formId}, (long)i, false));
            if (ot > 99 && ot < 104){
                int howManyOptions = rand.nextInt(7) + 1;
                for(int optionCount = 0; optionCount < howManyOptions; optionCount++){
                    long optionId = jdbcUtils.generateId();
                    form.add(new Element(optionId, elementId, Constants.OT_ELEMENT, generateOptionTitle(),
                            null, false, new Long[]{elementId,formId}, (long)optionCount, false));
                }
            }
        }
        long startTime = System.nanoTime();
        jdbcUtils.insertForm(form);
        long stopTime = System.nanoTime();
        System.out.println("0: " + (stopTime - startTime));
    }

    public void generateAndInsertQuesByFormId(long formId, int queCount) throws SQLException {
        JDBCUtils jdbcUtils = JDBCUtils.getInstance();
        Utils utils = new Utils();
        List<Element> form = jdbcUtils.getStudentFormById(formId, false);
        Random rand = new Random();
        for (int i = 0; i < queCount; i++) {
            List<Answer> answers = new ArrayList<>();
            long questionnaireId = jdbcUtils.generateId();
            for (Element question: form){
                long fieldId = question.getObjectId();
                int ot = (int)question.getOT();
                String value;
                switch (ot){
                    case 100: value = generateTextAnswer(); break;
                    case 104: value = generateDateAnswer(); break;
                    case 105: value = generateTextAnswer(); break;
                    default:{
                        if (ot > 100 && ot < 104){
                            List<Element> options = utils.findChildren(question, form);
                            value = options.size() > 0 ? options.get(rand.nextInt(options.size())).getName()
                                    : generateTextAnswer() ;
                        }
                        else value = generateTextAnswer();
                    }
                }
                if (ot > 99 && ot < 200) {
                    answers.add(new Answer(questionnaireId, fieldId, value, rand.nextBoolean()));
                }
            }
            jdbcUtils.insertStudentQuestionnaire(answers, formId);
            if (i % 100 == 0)
                System.out.println("*" + (i) + "*");
        }
    }

    private String generateText(){
        return "\"there is some text: " + UUID.randomUUID().toString() + "\"";
    }

    private String generateQuestionTitle(){
        return "Question title " + generateText();
    }

    private String generateQuestionDescription(){
        return "Question description " + generateText();
    }

    private String generateOptionTitle(){
        return "Option title " + generateText();
    }

    private String generateTextAnswer(){
        return "Answer text " + generateText();
    }

    private String generateDateAnswer() {
        return Calendar.getInstance().getTime().toString();
    }


}
