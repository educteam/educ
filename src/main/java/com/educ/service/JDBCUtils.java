package com.educ.service;

import com.educ.model.Answer;
import com.educ.model.Element;
import com.educ.model.QuestionnaireAnswer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static com.educ.service.Constants.*;

public class JDBCUtils {
    private static Logger log = Logger.getLogger(JDBCUtils.class.getName());
    private String DB_CONNECTION = "jdbc:postgresql://localhost:5432/EDUC"; //port and name of database
    private final String DB_USER = "postgres";
    private final String DB_PASSWORD = "netcracker"; //password for user postgres, is set while isntalling
    private final String DB_DRIVER = "org.postgresql.Driver";

    private final String EDUC_DATABASE_CREATION_FILE_PATH = "/db/EDUC Schema_database_create.sql";
    private final String EDUC_TABLES_CREATION_FILE_PATH = "/db/EDUC Schema_tables_create.sql";
    private final String EDUC_DATAFIX_DIRECTORY_PATH = "/db/datafixes";
    private static final String EDUC_QUERIES_STORAGE_DIRECTORY_PATH = "/db/queries/";//"src/main/resources/db/queries/";

    private static String getStudentFormByIdQuery;
    private static String insertObjectQuery;
    private static String deleteDeprecatedCuratorQueQuery;
    private static String getCuratorFormByStudQueIdQuery;
    private static String getFormFieldsByQueIdQuery;
    private static String getFieldsValuesByQueIdQuery;
    private static String getFieldsByFormIdQuery;
    private static String getCuratorFormFieldsByFormIdQuery;
    private static String getFieldsValuesByFormIdQuery;
    private static String getCuratorFormFieldsValuesByFormIdQuery;
    private static String getAllFormsQuery;
    private static String getFullFormByFormIdQuery;
    private static String updateObjectsQuery;
    private static String updateParamsQuery;
    private static String deleteQueQuery;
    private static String getAllQuesByStudQueIdQuery;

    private static final JDBCUtils instance = new JDBCUtils();
    static {
        try {
            getQueriesFromLocalStorage();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static JDBCUtils getInstance() {
        return instance;
    }

    private static void getQueriesFromLocalStorage() throws IOException {
        getStudentFormByIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getStudentFormByIdQuery.sql");
        insertObjectQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "insertObjectQuery.sql");
        deleteDeprecatedCuratorQueQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "deleteDeprecatedCuratorQueQuery.sql");
        getCuratorFormByStudQueIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getCuratorFormByStudQueIdQuery.sql");
        getFormFieldsByQueIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getFormFieldsByQueIdQuery.sql");
        getFieldsValuesByQueIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getFieldsValuesByQueIdQuery.sql");
        getFieldsByFormIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getFieldsByFormIdQuery.sql");
        getCuratorFormFieldsByFormIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getCuratorFormFieldsByFormIdQuery.sql");
        getFieldsValuesByFormIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getFieldsValuesByFormIdQuery.sql");
        getCuratorFormFieldsValuesByFormIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getCuratorFormFieldsValuesByFormIdQuery.sql");
        getAllFormsQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getAllFormsQuery.sql");
        getFullFormByFormIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getFullFormByFormIdQuery.sql");
        updateObjectsQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "updateObjectsQuery.sql");
        updateParamsQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "updateParamsQuery.sql");
        deleteQueQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "deleteQueQuery.sql");
        getAllQuesByStudQueIdQuery = Utils.readAllLinesFromFile(EDUC_QUERIES_STORAGE_DIRECTORY_PATH + "getAllQuesByStudQueIdQuery.sql");
    }

    private Connection getConnection() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException ex) {
            log.log(Level.SEVERE, "Include PostgreSQL JDBC Driver in your library path! Exception: ", ex);
            return null;
        }

        log.log(Level.FINE, "PostgreSQL JDBC Driver Registered!");
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (SQLException ex) {
            log.log(Level.SEVERE,"Connection Failed! Check output console: ", ex);
            return null;
        }
        return connection;
    }

    private static Long currentId;
    private static Long lastAllottedId;
    public synchronized long generateId() throws SQLException {
        if (currentId == null || currentId > lastAllottedId) {
            currentId = JDBCUtils.getInstance().getIdAndSetId();
            lastAllottedId = currentId + 1000;
        }
        return currentId++;
    }

    /*IMPORTANT FOR DEVELOPMENT. DON'T DELETE*/
    public void createEmptyDataBaseForBackup() throws IOException, SQLException {
        DB_CONNECTION = "jdbc:postgresql://localhost:5432/";
        String sql = Utils.readAllLinesFromFile(EDUC_DATABASE_CREATION_FILE_PATH);
        executeSQLCode(sql);
    }

    /*IMPORTANT FOR DEVELOPMENT. DON'T DELETE*/
    public void createEmptyDataBase() throws IOException, SQLException {
        createEmptyDataBaseForBackup();
        DB_CONNECTION += "EDUC";
        String sql = Utils.readAllLinesFromFile(EDUC_TABLES_CREATION_FILE_PATH);
        executeSQLCode(sql);
        executeAllScriptsFromDirectory(EDUC_DATAFIX_DIRECTORY_PATH);
    }

    private void executeSQLCode(String sql) throws SQLException {
        Connection dbConnection = getConnection();
        dbConnection.setAutoCommit(true);
        Statement statement = dbConnection.createStatement();
        statement.execute(sql);
        if (statement != null) {
            statement.close();
        }
        dbConnection.close();
    }

    private void executeAllScriptsFromDirectory(String dirPath) throws SQLException, IOException {
        List<File> fileList;
        File directory = new ClassPathResource(dirPath).getFile();
        if (directory.listFiles() == null) {
            return;
        }
        fileList = new ArrayList<>(Arrays.asList(directory.listFiles()));
        for (File file : fileList) {
            if (!Pattern.matches("^.*\\.sql$", file.getName())) {
                fileList.remove(file);
            } else {
                String sql = Utils.readAllLinesFromFile(dirPath +"\\" +file.getName());
                executeSQLCode(sql);
            }
        }
    }

    public List<Element> getStudentFormById(long objectId, Boolean withObsolete) throws SQLException {
        Connection dbConnection = getConnection();
        PreparedStatement getStudentFormByIdPS = dbConnection.prepareStatement(getStudentFormByIdQuery);
        getStudentFormByIdPS.setLong(1, IS_REQUIRED_ATTRIBUTE);
        getStudentFormByIdPS.setLong(2, ORDER_ATTRIBUTE);
        getStudentFormByIdPS.setLong(3, IS_OBSOLETE_ATTRIBUTE);
        getStudentFormByIdPS.setLong(4, objectId);
        getStudentFormByIdPS.setLong(5, Constants.OT_CURATOR_FORM);
        getStudentFormByIdPS.setLong(6, Constants.OT_HR_FORM);
        getStudentFormByIdPS.setLong(7, Constants.OT_MANAGER_FORM);
        return getFormByStatment(getStudentFormByIdPS, withObsolete);
    }

    private List<Element> getFormByStatment(PreparedStatement getFormPS, Boolean withObsolete) throws SQLException {
        List<Element> elements = new ArrayList<>();
        ResultSet rs = getFormPS.executeQuery();
        while (rs.next()){
            Element element = new Element(rs.getLong("object_id"), rs.getLong("parent_id"), rs.getLong("object_type_id"),
                    rs.getString("name"), rs.getString("description"), rs.getBoolean("is_required"),
                    rs.getInt("fields_order"), rs.getBoolean("is_obsolete"));
            if(rs.getString("field_value")!=null){
                element.setValue(rs.getString("field_value"));
            }
            if (!element.getObsolete() || withObsolete) {
                elements.add(element);
            }
        }
        getFormPS.getConnection().close();
        return elements;
    }

    private void insertQueElement(Element element, Connection dbConnectionWithAutoCommitOff) throws SQLException {
        if (element == null) {
            return;
        }
        StringBuilder transitionsInsertSql = new StringBuilder("INSERT INTO transition " +
                "(object_id, parent_id, level) VALUES ");
        List<Long> parentsIds = element.getParents();
        transitionsInsertSql.append("\n(" +
                element.getObjectId() + ", " +
                element.getObjectId() + ", " +
                0 + "),");
        if (parentsIds != null) {
            for (int j = 0; j < parentsIds.size(); j++) {
                transitionsInsertSql.append("\n(" +
                        element.getObjectId() + ", " +
                        parentsIds.get(j) + ", " +
                        (j + 1) + "),");
            }
        }
        transitionsInsertSql.deleteCharAt(transitionsInsertSql.length() - 1);

        StringBuilder paramsInsertSql = new StringBuilder("INSERT INTO params " +
                "(attr_id, object_id, value) VALUES ");
        int paramInsertCounter = 0;
        if (element.isRequired()) {
            paramsInsertSql.append("\n(" +
                    IS_REQUIRED_ATTRIBUTE + ", " +
                    element.getObjectId() + ",'" +
                    "true'),");
            paramInsertCounter++;
        }

        Long order = element.getOrder();
        if (order != null) {
            paramsInsertSql.append("\n(" +
                    ORDER_ATTRIBUTE + ", " +
                    element.getObjectId() + ",'" +
                    order + "'),");
            paramInsertCounter++;
        }
        paramsInsertSql.deleteCharAt(paramsInsertSql.length() - 1);

        PreparedStatement insertObjectPreparedStatement = dbConnectionWithAutoCommitOff.prepareStatement(insertObjectQuery);
        insertObjectPreparedStatement.setLong(1, element.getObjectId());
        insertObjectPreparedStatement.setLong(2, element.getOT());
        insertObjectPreparedStatement.setLong(3, element.getParentId());
        insertObjectPreparedStatement.setString(4, element.getName());
        insertObjectPreparedStatement.setString(5, element.getDescription());
        insertObjectPreparedStatement.execute();

        dbConnectionWithAutoCommitOff.createStatement().execute(transitionsInsertSql.toString());
        if (paramInsertCounter > 0) {
            dbConnectionWithAutoCommitOff.createStatement().execute(paramsInsertSql.toString());
        }

    }

    public void insertForm(List<Element> elements) throws SQLException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if(!securityMap.hasAdminGrants()) return;
        if ((elements != null) && (elements.size() != 0)) {
            StringBuilder objectsInsertSql = new StringBuilder("INSERT INTO objects " +
                    "(object_id, object_type_id, parent_id, name, description) VALUES ");
            StringBuilder transitionsInsertSql = new StringBuilder("INSERT INTO transition " +
                    "(object_id, parent_id, level) VALUES ");
            StringBuilder paramsInsertSql = new StringBuilder("INSERT INTO params " +
                    "(attr_id, object_id, value) VALUES ");
            for ( Element element: elements) {
                String elementName = StringEscapeUtils.escapeSql(element.getName());
                String elementDescription = element.getDescription() ==  null ? null : "'" + StringEscapeUtils.escapeSql(element.getDescription()) + "'";

                objectsInsertSql.append("\n(" +
                        element.getObjectId() + ", " +
                        element.getOT() + ", " +
                        element.getParentId() + ", '" +
                        elementName + "', " +
                        elementDescription + "),");

                List<Long> parentsIds = element.getParents();
                transitionsInsertSql.append("\n(" +
                        element.getObjectId() + ", " +
                        element.getObjectId() + ", " +
                        0 + "),");
                for (int j = 0; j < parentsIds.size(); j++) {
                    transitionsInsertSql.append("\n(" +
                            element.getObjectId() + ", " +
                            parentsIds.get(j) + ", " +
                            (j + 1) + "),");
                }

                paramsInsertSql.append("\n(" +
                        IS_REQUIRED_ATTRIBUTE + ", " +
                        element.getObjectId() + ",'" +
                        element.isRequired() + "'),");

                Long order = element.getOrder();
                order = order != null ? order : -1;

                paramsInsertSql.append("\n(" +
                        ORDER_ATTRIBUTE + ", " +
                        element.getObjectId() + ",'" +
                        order + "'),");

                paramsInsertSql.append("\n(" +
                        IS_OBSOLETE_ATTRIBUTE + ", " +
                        element.getObjectId() + ",'" +
                        "false'),");

            }
            objectsInsertSql.deleteCharAt(objectsInsertSql.length() - 1);
            transitionsInsertSql.deleteCharAt(transitionsInsertSql.length() - 1);
            paramsInsertSql.deleteCharAt(paramsInsertSql.length() - 1);

            Connection dbConnection = getConnection();
            dbConnection.setAutoCommit(false);
            dbConnection.createStatement().execute(objectsInsertSql.toString());
            dbConnection.createStatement().execute(transitionsInsertSql.toString());
            dbConnection.createStatement().execute(paramsInsertSql.toString());

            dbConnection.commit();
            dbConnection.setAutoCommit(true);
            dbConnection.close();
        }
    }

    public void updateStudentForm (List<Element> clientForm) throws SQLException {
        updateForm(clientForm, OT_STUDENT_FORM);
    }

    public void updateCuratorForm (List<Element> clientForm) throws SQLException {
       updateForm(clientForm, OT_CURATOR_FORM);
    }

    public void updateHRForm (List<Element> clientForm) throws SQLException {
        updateForm(clientForm, OT_HR_FORM);
    }

    public void updateForm(List<Element> clientForm, long formOT) throws SQLException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        if(!securityMap.hasAdminGrants()) return;
        long formId = -1;
        List<Element> listToUpdate = new ArrayList<>();
        List<Element> listToInsert = new ArrayList<>();
        for (Element element: clientForm){
            if (element.getOT() == formOT){
                formId = element.getObjectId();
                break;
            }
        }

        Connection dbConnection = getConnection();
        PreparedStatement getFullFormByFormIdPS = dbConnection.prepareStatement(getFullFormByFormIdQuery);
        getFullFormByFormIdPS.setLong(1, IS_REQUIRED_ATTRIBUTE);
        getFullFormByFormIdPS.setLong(2, ORDER_ATTRIBUTE);
        getFullFormByFormIdPS.setLong(3, IS_OBSOLETE_ATTRIBUTE);
        getFullFormByFormIdPS.setLong(4, formId);
        getFullFormByFormIdPS.setLong(5, formOT);
        getFullFormByFormIdPS.setLong(6, formId);
        List<Element> obsoleteForm = getFormByStatment(getFullFormByFormIdPS, true);
        for (Element element: clientForm){
            Element foundElementInObsoleteForm = findFieldInForm(element.getObjectId(), obsoleteForm);
            if (foundElementInObsoleteForm != null){
                listToUpdate.add(element);
                obsoleteForm.remove(foundElementInObsoleteForm); // to make search faster
            }else{
                listToInsert.add(element);
            }
        }

        insertForm(listToInsert);
        updateExistingElements(listToUpdate);
    }

    private void updateExistingElements(List<Element> elementsForUpdate) throws SQLException {
        if (elementsForUpdate.size() != 0) {
            StringBuilder updateObjectsValues = new StringBuilder();
            StringBuilder updateParamsValues = new StringBuilder();
            for (Element element : elementsForUpdate) {
                String elementDescription = element.getDescription() == null ? null : "'" + element.getDescription() + "'";
                Long order = element.getOrder() != null ? element.getOrder() : -1;
                updateObjectsValues.append("\n\t\t(" + element.getObjectId() + ",'" + element.getName() + "'," + elementDescription + "),");
                updateParamsValues.append("\n\t\t(" + IS_REQUIRED_ATTRIBUTE + "," + element.getObjectId() + ",'" + element.isRequired() + "'),");
                updateParamsValues.append("\n\t\t(" + IS_OBSOLETE_ATTRIBUTE + "," + element.getObjectId() + ",'" + element.getObsolete() + "'),");
                updateParamsValues.append("\n\t\t(" + ORDER_ATTRIBUTE + "," + element.getObjectId() + ",'" + order + "'),");
            }
            updateObjectsValues.deleteCharAt(updateObjectsValues.length() - 1);
            updateParamsValues.deleteCharAt(updateParamsValues.length() - 1);

            Connection dbConnection = getConnection();
            dbConnection.setAutoCommit(false);
            String updateObjectsValuesQuery = String.format(updateObjectsQuery, updateObjectsValues.toString());
            String updateParamsValuesQuery = String.format(updateParamsQuery, updateParamsValues.toString());
            dbConnection.createStatement().execute(updateObjectsValuesQuery);
            dbConnection.createStatement().execute(updateParamsValuesQuery);
            dbConnection.commit();
            dbConnection.close();
        }
    }

    private Element findFieldInForm(long fieldId, List<Element> form) {
        for (Element element: form){
            if (fieldId == element.getObjectId()){
                return element;
            }
        }
        return null;
    }


    public void insertStudentQuestionnaire(List<Answer> answers, Long parentId) throws SQLException {
        insertQuestionnaire(answers, parentId, OT_STUDENT_QUE, false);
    }

    public void insertCuratorQuestionnaire(List<Answer> answers, Long parentId) throws SQLException {
        insertQuestionnaire(answers, parentId, OT_CURATOR_QUE, false);
    }

    public void insertHRQuestionnaire(List<Answer> answers, Long parentId) throws SQLException {
        insertQuestionnaire(answers, parentId, OT_HR_QUE, false);
    }

    private boolean checkQueIsValid(List<Answer> answers, Long parentId, long questionnaireOT) throws SQLException {
        List<Element> form = null;
        Utils utils = new Utils();
        switch ((int) questionnaireOT){
            case ((int) OT_STUDENT_QUE): form = getStudentFormById(parentId, false);
                break;
            case ((int) OT_CURATOR_QUE): form = getCuratorFormByStudentQuestionnaireId(parentId, false);
                break;
            case ((int) OT_HR_QUE): form = getHRFormByStudentQuestionnaireId(parentId, false);
                break;
        }
        Boolean queIsValid = true;
        int questionCounter = 0;

        if (form == null){
            throw new RuntimeException("Form wasn't found!");
        }
        for (Element formField : form) {
            if (!queIsValid) { break; }

            if (formField.getOT() < 100 || formField.getOT() > 200 || formField.getObsolete()) {
                continue;
            }

            Answer answer = formField.findRelatedAnswer(answers);
            if (answer == null || answer.isEmpty()) {
                queIsValid = !formField.isRequired();
                questionCounter++;
                continue;
            }

            String pattern = null;
            switch ((int) formField.getOT()) {
                case (int) OT_EMAIL:
                    pattern = EMAIL_PATTERN;
                    break;
                case (int) OT_BOUNDED_NUMBER:
                    pattern = NUMBER_PATTERN;
                    break;
                case (int) OT_YEAR_OF_BIRTH:
                    pattern = NUMBER_PATTERN;
                    break;
                case (int) OT_DATE:
                    answer.editValue(answer.getValue().replace("T21:00:00.000Z", ""));
                    pattern = DATE_PATTERN;
                    break;
                case (int) OT_PHONE_NUMBER:
                    pattern = PHONE_PATTERN;
                    break;
            }
            /*For all other fields there is no regexp*/
            queIsValid = pattern == null || answer.getValue().matches(pattern);

            if (formField.getOT() == OT_YEAR_OF_BIRTH) {
                queIsValid = utils.tryParseInt(answer.getValue());
                if (queIsValid) {
                    Integer yearOfBirth = Integer.parseInt(answer.getValue());
                    queIsValid = (MIN_DATE_OF_BIRTH <= yearOfBirth && yearOfBirth <= MAX_DATE_OF_BIRTH);
                }
            }
            questionCounter++;
        }
        /*User shouldn't send more answers then form contains*/
        Set<Long> distinctAnswerIds = new HashSet<>();
        for (Answer answer: answers){
            distinctAnswerIds.add(answer.getFieldId());
        }

        if (questionCounter != distinctAnswerIds.size()){
            queIsValid = false;
        }

        return queIsValid;
    }

    private void insertQuestionnaire(List<Answer> answers, Long parentId, long questionnaireOT, boolean withoutCheck) throws SQLException {
        if ((answers != null) && (answers.size() != 0)) {
            long questionnaireId = answers.get(0).getQuestionnaireId();
            if( !withoutCheck && !checkQueIsValid(answers, parentId, questionnaireOT)){
                throw new IllegalArgumentException("One of the fields filled incorrect");
            }

            Connection dbConnection = getConnection();
            dbConnection.setAutoCommit(false);

            /* To delete previous curator questionnaire if exist */
            if ( questionnaireOT != Constants.OT_STUDENT_QUE){
                PreparedStatement preparedStatement = dbConnection.prepareStatement(deleteDeprecatedCuratorQueQuery);
                preparedStatement.setLong(1, parentId);
                preparedStatement.setLong(2, questionnaireOT);
                preparedStatement.execute();
            }

            insertQueElement(new Element(questionnaireId, parentId, questionnaireOT, String.valueOf(questionnaireId)), dbConnection);
            StringBuilder answersInsertQuery = new StringBuilder("INSERT INTO params (attr_id, object_id, value) VALUES ");
            for (Answer answer: answers) {
                String value = answer.getValue() != null ? answer.getValue() : "";
                value = StringEscapeUtils.escapeSql(value);
                answersInsertQuery.append("\n(" + answer.getFieldId() + ", " + answer.getQuestionnaireId() +
                        ",'" + value + "'),");
            }
            answersInsertQuery.deleteCharAt(answersInsertQuery.length() - 1);

            dbConnection.createStatement().execute(answersInsertQuery.toString());
            if ( questionnaireOT == Constants.OT_STUDENT_QUE){
                List<Answer> curatorFakeAnswers = generateEmployeeEmptyQue(dbConnection, parentId, OT_CURATOR_FORM);
                List<Answer> hrFakeAnswers = generateEmployeeEmptyQue(dbConnection, parentId, OT_HR_FORM);
                List<Answer> managerFakeAnswers = generateEmployeeEmptyQue(dbConnection, parentId, OT_MANAGER_FORM);
                dbConnection.commit();
                dbConnection.close();
                insertQuestionnaire(curatorFakeAnswers, questionnaireId, OT_CURATOR_QUE, true);
                insertQuestionnaire(hrFakeAnswers, questionnaireId, OT_HR_QUE, true);
                insertQuestionnaire(managerFakeAnswers, questionnaireId, OT_MANAGER_QUE, true);
            }else {
                dbConnection.commit();
                dbConnection.close();
            }
        }
    }

    private List<Answer> generateEmployeeEmptyQue(Connection dbConnection, long parentId, long formOT) throws SQLException {
        LinkedHashMap<Long, String> employeeFormFields = getEmployeeFormFieldsByFormId(dbConnection, parentId, formOT, new ArrayList<>());
        List<Answer> curatorFakeAnswers = new ArrayList<>();
        long curatorFakeQueId = generateId();
        for (Long curatorAttr: employeeFormFields.keySet()){
            curatorFakeAnswers.add(new Answer(curatorFakeQueId, curatorAttr, "", false));
        }
        return curatorFakeAnswers;
    }

    public List<Element> getCuratorFormByStudentQuestionnaireId(long questionnairesId, boolean isForUpdate) throws SQLException {
        return getEmployeeFormByStudentQueId(questionnairesId, OT_CURATOR_FORM, OT_CURATOR_QUE, isForUpdate);
    }

    public List<Element> getHRFormByStudentQuestionnaireId(long questionnairesId, boolean isForUpdate) throws SQLException {
        return getEmployeeFormByStudentQueId(questionnairesId, OT_HR_FORM, OT_HR_QUE, isForUpdate);
    }

    public List<Element> getManagerFormByStudentQuestionnaireId(long questionnairesId, boolean isForUpdate) throws SQLException {
        return getEmployeeFormByStudentQueId(questionnairesId, OT_MANAGER_FORM, OT_MANAGER_QUE, isForUpdate);
    }

    public List<Element> getEmployeeFormByStudentQueId(long questionnairesId, long formOT, long queOT, boolean isForUpdate) throws SQLException {
        List<Element> employeeForm = new ArrayList<>();
        PreparedStatement getCuratorFormPS = getConnection().prepareStatement(getCuratorFormByStudQueIdQuery);
        getCuratorFormPS.setLong(1, IS_REQUIRED_ATTRIBUTE);
        getCuratorFormPS.setLong(2, ORDER_ATTRIBUTE);
        getCuratorFormPS.setLong(3, questionnairesId);
        getCuratorFormPS.setLong(4, queOT);
        getCuratorFormPS.setLong(5, IS_OBSOLETE_ATTRIBUTE);
        getCuratorFormPS.setLong(6, questionnairesId);
        getCuratorFormPS.setLong(7, formOT);
        employeeForm.addAll(getFormByStatment(getCuratorFormPS, isForUpdate));
        return employeeForm;
    }
    public List<QuestionnaireAnswer> getStudentQueById(long questionnaireId) throws SQLException {
        return getStudentQueById(questionnaireId, new ArrayList<>());
    }

    public List<QuestionnaireAnswer> getStudentQueById(long questionnaireId, List<String> employeeRoles) throws SQLException {
        List<QuestionnaireAnswer> questionnaireAnswers = new ArrayList<>();

        Connection dbConnection = getConnection();
        dbConnection.setAutoCommit(false);
        PreparedStatement formFieldPS = dbConnection.prepareStatement(getFormFieldsByQueIdQuery);
        formFieldPS.setLong(1, ORDER_ATTRIBUTE);
        formFieldPS.setLong(2, OT_STUDENT_QUE);
        formFieldPS.setLong(3, questionnaireId);
        LinkedHashMap<Long, String> existingFields = getExistingFieldsByStatement(formFieldPS, new ArrayList<>());

        PreparedStatement formFieldValuesPS = dbConnection.prepareStatement(getFieldsValuesByQueIdQuery);
        formFieldValuesPS.setLong(1, questionnaireId);
        formFieldValuesPS.setLong(2, OT_STUDENT_QUE);
        LinkedHashMap<Long, LinkedHashMap<Long, String>> fieldsValues = getQuesByStatment(formFieldValuesPS);

        Set<Long> existingFieldsKeys = existingFields.keySet();
        for (Long existingFieldsKey : existingFieldsKeys) {
            String value = fieldsValues.get(questionnaireId).get(existingFieldsKey);
            value = value != null ? value : "-";
            questionnaireAnswers.add(new QuestionnaireAnswer(existingFields.get(existingFieldsKey), value));
        }

        dbConnection.close();
        return questionnaireAnswers;
    }

    public Map<Long, String> getAllQuesByStudQueId(Long studentQueId) throws SQLException {
        Connection dbConnection = getConnection();
        Array employeeQueOTsSqlArray = dbConnection.createArrayOf("bigint", new Long[]{OT_CURATOR_QUE, OT_HR_QUE});
        Array employeeFormOTsSqlArray = dbConnection.createArrayOf("bigint", new Long[]{OT_CURATOR_FORM, OT_HR_FORM});
        PreparedStatement getEmployeeFormFieldsByFormPS = dbConnection.prepareStatement(getAllQuesByStudQueIdQuery);
        getEmployeeFormFieldsByFormPS.setLong(1, studentQueId);
        getEmployeeFormFieldsByFormPS.setArray(2, employeeQueOTsSqlArray);
        getEmployeeFormFieldsByFormPS.setLong(3, studentQueId);
        getEmployeeFormFieldsByFormPS.setArray(4, employeeFormOTsSqlArray);
        getEmployeeFormFieldsByFormPS.setLong(5, OT_STUDENT_QUE);
        getEmployeeFormFieldsByFormPS.setLong(6, studentQueId);

        Map<Long, String> answersMap = new HashMap<>();
        ResultSet rs = getEmployeeFormFieldsByFormPS.executeQuery();
        while (rs.next()) {
            Long fieldId = rs.getLong("field_id");
            String otherAnswers = answersMap.get(fieldId);
            String answer = otherAnswers != null ? (otherAnswers + ", " + rs.getString("field_value")) : rs.getString("field_value");
            answersMap.put(fieldId, answer);
        }
        dbConnection.close();
        return answersMap;
    }

    private LinkedHashMap<Long, LinkedHashMap<Long, String>> getEmployeeFormFields(Connection dbConnection,
                                                                                   long formId,
                                                                                   long employeeFormOT,
                                                                                   String employeeValueFilterSql) throws SQLException {
        PreparedStatement getEmployeeValuesByFIdPS =
                dbConnection.prepareStatement(String.format(getCuratorFormFieldsValuesByFormIdQuery, employeeValueFilterSql));
        getEmployeeValuesByFIdPS.setLong(1, formId);
        getEmployeeValuesByFIdPS.setLong(2, employeeFormOT);

        return getQuesByStatment(getEmployeeValuesByFIdPS);
    }

    public JSONObject getCompositeQuestionnaireTableByFormId(long formId) throws SQLException {
        return getCompositeQuestionnaireTableByFormId(formId, new HashMap<>(), new ArrayList<>());
    }

    public JSONObject getCompositeQuestionnaireTableByFormId(long formId, Map<String, Map<Long, String>> rolesFilterFields) throws SQLException {
        return getCompositeQuestionnaireTableByFormId(formId, rolesFilterFields, new ArrayList<>());
    }

    /**
     * The method is used for three types of operation: 1 - getting full composite table of answers for chosen
     * form and current role, 2 - getting answers of particular questionnaires according to filters, 3 - getting values
     * of particular fields for particular questionnaires for export to xls.
     * @param formId - id of current form (Required for each mode ) (required for operations: 1, 2, 3)
     * @param rolesFilterFields - map of filters for fields of each form () (required for operations: 2, 3)
     * @param shownFieldsFilter - map of fields which user can see atg the moment (required for operation: 3)
     * @return composite table as JSON
     * @throws SQLException
     */
    JSONObject getCompositeQuestionnaireTableByFormId(long formId, Map<String, Map<Long, String>> rolesFilterFields,
                                                      List<Long> shownFieldsFilter) throws SQLException {
        CurrentSecurityMap securMap = new CurrentSecurityMap();
        LinkedHashMap<Long, String> existCuratorFields = new LinkedHashMap<>();
        LinkedHashMap<Long, String> existHRFields = new LinkedHashMap<>();
        LinkedHashMap<Long, LinkedHashMap<Long, String>> curatorFormFieldsValues = new LinkedHashMap<>();
        LinkedHashMap<Long, LinkedHashMap<Long, String>> hrFormFieldsValues = new LinkedHashMap<>();

        String studentValueFilterSql = createValueFilterSql(rolesFilterFields.get(ROLE_STUDENT));
        String curatorValueFilterSql = securMap.hasCuratorGrants() ? createValueFilterSql(rolesFilterFields.get(ROLE_CURATOR)) : "";
        String hrValueFilterSql = securMap.hasHRGrants() ? createValueFilterSql(rolesFilterFields.get(ROLE_HR)) : "";

        Connection dbConnection = getConnection();
        dbConnection.setAutoCommit(false);
        PreparedStatement getFieldsValuesByFormIdPS = dbConnection.prepareStatement(String.format(getFieldsValuesByFormIdQuery, studentValueFilterSql));
        getFieldsValuesByFormIdPS.setLong(1, formId);
        LinkedHashMap<Long, LinkedHashMap<Long, String>> studentFieldsValues = getQuesByStatment(getFieldsValuesByFormIdPS);

        PreparedStatement getFieldsByFormIdPS = dbConnection.prepareStatement(getFieldsByFormIdQuery);
        getFieldsByFormIdPS.setLong(1, ORDER_ATTRIBUTE);
        getFieldsByFormIdPS.setLong(2, formId);
        LinkedHashMap<Long, String> existStudentFields = getExistingFieldsByStatement(getFieldsByFormIdPS, shownFieldsFilter);

        if (securMap.hasCuratorGrants()) {
            curatorFormFieldsValues = getEmployeeFormFields(dbConnection, formId, OT_CURATOR_FORM, curatorValueFilterSql);
            existCuratorFields = getEmployeeFormFieldsByFormId(dbConnection, formId, OT_CURATOR_FORM, shownFieldsFilter);
        }
        if (securMap.hasHRGrants()) {
            hrFormFieldsValues = getEmployeeFormFields(dbConnection, formId, OT_HR_FORM, hrValueFilterSql);
            existHRFields = getEmployeeFormFieldsByFormId(dbConnection, formId, OT_HR_FORM, shownFieldsFilter);
        }
        dbConnection.close();

        JSONArray studentFormTitles = convertHashMapTitlesToJSONArray(existStudentFields);
        Set<Long> studQueIds = studentFieldsValues.keySet();
        JSONArray curatorFormTitles = new JSONArray();
        if (securMap.hasCuratorGrants()) {
            curatorFormTitles = convertHashMapTitlesToJSONArray(existCuratorFields);
            Set<Long> curatorQuestionnairesIds = curatorFormFieldsValues.keySet();
            studQueIds.retainAll(curatorQuestionnairesIds);
        }
        JSONArray hrFormTitles = new JSONArray();
        if (securMap.hasHRGrants()) {
            hrFormTitles = convertHashMapTitlesToJSONArray(existHRFields);
            Set<Long> hrQuestionnairesIds = hrFormFieldsValues.keySet();
            studQueIds.retainAll(hrQuestionnairesIds);
        }

        JSONArray answers = new JSONArray();
        for (Long questionnaireId : studQueIds) {
            /*If user doesn't have enough grants studentFieldsValues is empty*/
            JSONArray studentAnswers = getAnswersFromHashMap(studentFieldsValues, getNotNullSet(existStudentFields), questionnaireId);
            JSONArray curatorAnswers = getAnswersFromHashMap(curatorFormFieldsValues, getNotNullSet(existCuratorFields), questionnaireId);
            JSONArray hrAnswers = getAnswersFromHashMap(hrFormFieldsValues, getNotNullSet(existHRFields), questionnaireId);
            JSONObject answer = new JSONObject().put("id", questionnaireId).put("studentAnswers", studentAnswers)
                    .put("curatorAnswers", curatorAnswers).put("hrAnswers", hrAnswers);
            answers.put(answer);
        }
        JSONObject result = new JSONObject();
        result.put("titles", studentFormTitles);
        result.put("curatorTitles", curatorFormTitles);
        result.put("hrTitles", hrFormTitles);
        result.put("answers", answers);

        return result;
    }

    private LinkedHashMap<Long, String> getEmployeeFormFieldsByFormId(Connection dbConnection, long formId,
                                                                      long formOT, List<Long> shownFieldsFilter) throws SQLException {
        PreparedStatement getCuratorFormFieldsByFormIdPS = dbConnection.prepareStatement(getCuratorFormFieldsByFormIdQuery);
        getCuratorFormFieldsByFormIdPS.setLong(1, ORDER_ATTRIBUTE);
        getCuratorFormFieldsByFormIdPS.setLong(2, formId);
        getCuratorFormFieldsByFormIdPS.setLong(3, formOT);
        return getExistingFieldsByStatement(getCuratorFormFieldsByFormIdPS, shownFieldsFilter);
    }

    private String createValueFilterSql(Map<Long, String> roleFilterFields){
        if (roleFilterFields != null) {
            int i = 0;
            StringBuilder valueFilterSql = new StringBuilder();
            for (long roleFilterFieldAttrId : roleFilterFields.keySet()) {
                String filterValue = roleFilterFields.get(roleFilterFieldAttrId);
                if (!filterValue.isEmpty()) {
                    valueFilterSql.append(String.format("JOIN params p%d ON (p%d.attr_id = %d AND p%d.object_id =" +
                                    " param.object_id AND upper(p%d.value) LIKE upper('%%%s%%'))%n", i, i, roleFilterFieldAttrId, i, i++,
                            StringEscapeUtils.escapeSql(filterValue)));
                }
            }
            return valueFilterSql.toString();
        }else {
            return "";
        }
    }

    private JSONArray convertHashMapTitlesToJSONArray(LinkedHashMap<Long, String> existingEmployeeFormFields){
        JSONArray employeeFormTitles = new JSONArray();
        if (existingEmployeeFormFields != null) {
            Set<Long> existingEmployeeFormFieldsKeys = existingEmployeeFormFields.keySet();
            for (Long existingFieldsKey : existingEmployeeFormFieldsKeys) {
                JSONObject title = new JSONObject().put("field_id", existingFieldsKey)
                        .put("value", existingEmployeeFormFields.get(existingFieldsKey));
                employeeFormTitles.put(title);
            }
        }
        return employeeFormTitles;
    }

    private Set<Long> getNotNullSet(LinkedHashMap<Long, String> existingEmployeeFormFields){
        Set<Long> existingEmployeeFormFieldsKeys = new HashSet<>();
        if (existingEmployeeFormFields != null) {
            existingEmployeeFormFieldsKeys = existingEmployeeFormFields.keySet();
        }
        return existingEmployeeFormFieldsKeys;
    }


    private JSONArray getAnswersFromHashMap(LinkedHashMap<Long, LinkedHashMap<Long, String>> fieldsValues,
                                     Set<Long> existingFieldsKeys, Long questionnaireId){
        JSONArray answers = new JSONArray();
        for (Long existingFieldsKey: existingFieldsKeys) {
            JSONObject questionnairesValues = new JSONObject();
            String value;
            if(fieldsValues.get(questionnaireId) != null) {
                value = fieldsValues.get(questionnaireId).get(existingFieldsKey);
                value = (value != null && !value.isEmpty()) ? value : "-";
            }
            else value = "-";
            questionnairesValues.put("field_id",existingFieldsKey);
            questionnairesValues.put("field_value",value);
            answers.put(questionnairesValues);
        }
        return answers;
    }

    private LinkedHashMap<Long, String> getExistingFieldsByStatement(PreparedStatement preparedStatement,
                                                                     List<Long> shownFieldsFilter) throws SQLException {
        LinkedHashMap<Long, String> existingFields = new LinkedHashMap<>();
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Long fieldId = rs.getLong("field_id");
            String name = rs.getString("field_name");
            if ((securityMap.hasHRGrants() || !ONLY_FOR_HR_FIELD_IDS.contains(fieldId)) &&
                    (CollectionUtils.isEmpty(shownFieldsFilter) || shownFieldsFilter.contains(fieldId))) {
                existingFields.put(fieldId, name);
            }
        }
        return existingFields;
    }

    private LinkedHashMap<Long, LinkedHashMap<Long, String>> getQuesByStatment(PreparedStatement preparedStatement) throws SQLException {
        CurrentSecurityMap securityMap = new CurrentSecurityMap();
        ResultSet rs = preparedStatement.executeQuery();
        Long prevQuestionnaireId = Long.MIN_VALUE;
        LinkedHashMap<Long, String> currentQuestionnaireMap = new LinkedHashMap<>();
        //LinkedHashMap<Long, String> existFieldsMap = new LinkedHashMap<>();
        LinkedHashMap<Long, LinkedHashMap<Long, String>> fieldsValues = new LinkedHashMap<>();
        while (rs.next()) {
            Long studQuestionnaireId = rs.getLong("stud_questionnaire");
            Long questionnaireId = (studQuestionnaireId != -1L) ? studQuestionnaireId : rs.getLong("questionnaire_id");
            if(!prevQuestionnaireId.equals(questionnaireId)){
                currentQuestionnaireMap = new LinkedHashMap<>();
            }
            if (studQuestionnaireId != -1L) {
                prevQuestionnaireId = studQuestionnaireId;
            }else {
                prevQuestionnaireId = questionnaireId;
            }
            Long fieldId = rs.getLong("field_id");
            String fieldValue = rs.getString("field_value");
            String fieldName = rs.getString("field_name");
            if (securityMap.hasHRGrants() || !ONLY_FOR_HR_FIELD_IDS.contains(fieldId)){
                //existFieldsMap.put(fieldId, fieldName);
                if(currentQuestionnaireMap.get(fieldId) != null){
                    fieldValue = currentQuestionnaireMap.get(fieldId) + ", " + fieldValue;
                }
                currentQuestionnaireMap.put(fieldId, fieldValue);
                fieldsValues.put(prevQuestionnaireId, currentQuestionnaireMap);
            }
        }
        //fieldsValues.put(-1l, existFieldsMap);
        return fieldsValues;
    }

    public Map<Long, Map<String, String>> getAllForms() throws SQLException {
        Map<Long, Map<String, String>> formsMap = new HashMap<>();
        Map<String, String> formMap = new HashMap<>();
        Connection dbConnection = getConnection();
        PreparedStatement getAllFormsPS = dbConnection.prepareStatement(getAllFormsQuery);
        getAllFormsPS.setLong(1, OT_STUDENT_QUE);
        getAllFormsPS.setLong(2, OT_STUDENT_FORM);
        ResultSet rs = getAllFormsPS.executeQuery();
        while (rs.next()) {
            Long formId = rs.getLong(Constants.FORM_ID);
            formMap.put(Constants.FORM_ID, String.valueOf(formId));
            formMap.put(Constants.FORM_NAME, rs.getString(Constants.FORM_NAME));
            formMap.put(Constants.QUESTIONNAIRES_COUNT, String.valueOf(rs.getLong(Constants.QUESTIONNAIRES_COUNT)));
            formsMap.put(formId ,formMap);
            formMap = new HashMap<>();
        }

        dbConnection.close();
        return formsMap;
    }

    public void deleteQueById(long id) throws SQLException {
        Connection dbConnection = getConnection();
        dbConnection.setAutoCommit(true);
        Statement getQueStatment = dbConnection.createStatement();
        ResultSet rs = getQueStatment.executeQuery(String.format("SELECT o.object_id FROM objects o" +
                " WHERE o.object_id = %d and o.object_type_id = %d", id, OT_STUDENT_QUE));
        rs.next();
        Long queId = rs.getLong("object_id");
        PreparedStatement deleteQuePS = dbConnection.prepareStatement(deleteQueQuery);
        deleteQuePS.setLong(1, id);
        deleteQuePS.setLong(2, id);
        deleteQuePS.execute();
        dbConnection.close();
    }

    private long getIdAndSetId() throws SQLException {
        long lastId = getLastIdFromTable("ids");
        PreparedStatement setIdPS = getConnection().prepareStatement("INSERT INTO ids (current_id) VALUES ( ? )");
        setIdPS.setLong(1, lastId + 1001);
        setIdPS.execute();
        setIdPS.getConnection().close();
        return ++lastId;
    }

    private long getLastIdFromTable(String table) throws SQLException {
        long lastId = 0;
        table = table.toUpperCase();

        String tableId = Constants.TABLE_2_ID.get(table);
        if (tableId == null) {
            throw new IllegalArgumentException("Unknown table");
        }
        PreparedStatement getLastIdPS = getConnection().
                prepareStatement("SELECT " + tableId + " FROM " + table + " ORDER BY " + tableId + " DESC LIMIT 1");

        ResultSet rs = getLastIdPS.executeQuery();
        if (rs.next()) {
            long lastIndex = rs.getLong(tableId);
            lastId = lastIndex + 1;
        }
        getLastIdPS.getConnection().close();
        return lastId;
    }

}
