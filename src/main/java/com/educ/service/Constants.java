package com.educ.service;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by GriGri on 01.11.2016.
 */
public class Constants {
    public static final BidiMap OBJECT_TYPES = new DualHashBidiMap();
    public static final Map<String, String> TABLE_2_ID = new HashMap<>();

    static {
        OBJECT_TYPES.put("form", 1L);

        OBJECT_TYPES.put("element", 8L);
        OBJECT_TYPES.put("questionnaire", 9L);
        OBJECT_TYPES.put("attribute", 10L);
        OBJECT_TYPES.put("text", 100L);
        OBJECT_TYPES.put("checkbox", 101L);
        OBJECT_TYPES.put("radio", 102L);
        OBJECT_TYPES.put("list", 103L);
        OBJECT_TYPES.put("date", 104L);
        OBJECT_TYPES.put("textarea", 105L);
        OBJECT_TYPES.put("phone_number", 106L);
        OBJECT_TYPES.put("email", 107L);
        OBJECT_TYPES.put("bounded_number", 108L);
        OBJECT_TYPES.put("year_of_birth", 109L);

        TABLE_2_ID.put("OBJECTS", "OBJECT_ID");
        TABLE_2_ID.put("PARAMS", "PARAM_ID");
        TABLE_2_ID.put("OBJECT_TYPES", "OBJECT_TYPE_ID");
        TABLE_2_ID.put("IDS", "CURRENT_ID");
    }


    public static final long IS_REQUIRED_ATTRIBUTE = 1002L;
    public static final long IS_OBSOLETE_ATTRIBUTE = 1003L;
    public static final long ORDER_ATTRIBUTE = 1006L;

    public static final long OT_STUDENT_FORM = 1L;
    public static final long OT_ELEMENT = 8L;
    public static final long OT_STUDENT_QUE = 9L;
    public static final long OT_CURATOR_FORM = 11L;
    public static final long OT_CURATOR_QUE = 12L;
    public static final long OT_HR_FORM = 13L;
    public static final long OT_HR_QUE = 14L;
    public static final long OT_MANAGER_FORM = 15L;
    public static final long OT_MANAGER_QUE = 16L;
    public static final long OT_DATE = 104L;
    public static final long OT_PHONE_NUMBER = 106L;
    public static final long OT_EMAIL = 107L;
    public static final long OT_BOUNDED_NUMBER = 108L;
    public static final long OT_YEAR_OF_BIRTH = 109L;

    public static final String FORM_ID = "form_id";
    public static final String FORM_NAME = "form_name";
    public static final String QUESTIONNAIRES_COUNT = "quests_count";

    public static final String STUDENT_FORM_NAME = "STUDENT_FORM";
    public static final String CURATOR_FORM_NAME = "CURATOR_FORM";
    public static final String HR_FORM_NAME = "HR_FORM";
    public static final String MANAGER_FORM_NAME = "MANAGER_FORM";

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_CURATOR = "ROLE_CURATOR";
    public static final String ROLE_HR = "ROLE_HR";
    public static final String ROLE_MANAGER = "ROLE_MANAGER";
    public static final String ROLE_STUDENT = "ROLE_STUDENT";

    public static final String START_MARK = "{{$";
    public static final String END_MARK = "}}";

    public static final String EMAIL_PATTERN = "^.+@.+$";
    public static final String PHONE_PATTERN = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$";
    public static final String NUMBER_PATTERN = "^[-+]?\\d+(\\.\\d+)?$";
    public static final String DATE_PATTERN = "^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$";
    public static final Integer MIN_DATE_OF_BIRTH = 1970;
    public static final Integer MAX_DATE_OF_BIRTH = 2001;
    public static final ArrayList<Long> ONLY_FOR_HR_FIELD_IDS =
            new ArrayList(Arrays.asList(new Long[]{2010l, 2011l, 2012l, 2013l, 2014l, 2015l, 6059l, 6067l,
                    20377l, 20379l, 20380l, 20378l, 20381l, 20382l}));

}
