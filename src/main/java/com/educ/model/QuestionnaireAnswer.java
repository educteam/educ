package com.educ.model;

/**
 * Created by INDIGO-ПС on 24.11.2016.
 */
public class QuestionnaireAnswer {

    private String title ;
    private String answer;

    public QuestionnaireAnswer(String title, String answer) {
        this.title = title;
        this.answer = answer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
