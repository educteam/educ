package com.educ.model.filter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilteredDataRequest {
    private Long formId;
    private List<Filter> studentFilters;
    private List<Filter> curatorFilters;
    private List<Filter> hrFilters;
    private List<Long> shownFields;

    public Long getFormId() {
        return formId;
    }

    public List<Filter> getStudentFilters() {
        return studentFilters;
    }

    public List<Filter> getCuratorFilters() {
        return curatorFilters;
    }

    public List<Filter> getHrFilters() {
        return hrFilters;
    }

    public List<Long> getShownFields() {
        return shownFields;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public void setStudentFilters(List<Filter> studentFilters) {
        this.studentFilters = studentFilters;
    }

    public void setCuratorFilters(List<Filter> curatorFilters) {
        this.curatorFilters = curatorFilters;
    }

    public void setHrFilters(List<Filter> hrFilters) {
        this.hrFilters = hrFilters;
    }

    public void setShownFields(List<Long> shownFields) {
        this.shownFields = shownFields;
    }

    public HashMap<Long, String> getStudentFiltersAsHashMap(){
        HashMap<Long, String> result = new HashMap<Long, String>();
        for (Filter filter: studentFilters){
            result.put(filter.getId(), filter.getValue());
        }
        return result;
    }

    public HashMap<Long, String> getCuratorFiltersAsHashMap(){
        HashMap<Long, String> result = new HashMap<Long, String>();
        for (Filter filter: curatorFilters){
            result.put(filter.getId(), filter.getValue());
        }
        return result;
    }

    public HashMap<Long, String> getHRFiltersAsHashMap(){
        HashMap<Long, String> result = new HashMap<Long, String>();
        for (Filter filter: hrFilters){
            result.put(filter.getId(), filter.getValue());
        }
        return result;
    }

}
