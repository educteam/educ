package com.educ.model.generationForm;

import java.util.List;

public class UIForm extends UIElement {

    /*The field isn't null only for curator form*/
    private Long dependentQueId;
    private String objectTypeName;
    public UIForm(){}

    /*Constructor for curator forms*/
    public UIForm(Long dependentQueId){
        this.dependentQueId = dependentQueId;
    }

    private List<FormField> form_fields;

    public List<FormField> getForm_fields() {
        return form_fields;
    }

    public void setForm_fields(List<FormField> form_fields) {
        this.form_fields = form_fields;
    }

    public Long getDependentQueId() {
        return dependentQueId;
    }

    public String getObjectTypeName() {
        return objectTypeName;
    }

    public void setObjectTypeName(String objectTypeName) {
        this.objectTypeName = objectTypeName;
    }

    public boolean isCuratorial(){
        if (dependentQueId != null)
            return true;
        return false;
    }
}
