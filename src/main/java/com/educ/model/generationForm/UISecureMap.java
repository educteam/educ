package com.educ.model.generationForm;

public class UISecureMap {
    private Boolean hasAdminGrants;
    private Boolean hasCuratorGrants;
    private Boolean hasHRGrants;

    public UISecureMap(Boolean hasCuratorGrants, Boolean hasHRGrants, Boolean hasAdminGrants){
        this.hasCuratorGrants = hasCuratorGrants;
        this.hasHRGrants = hasHRGrants;
        this.hasAdminGrants = hasAdminGrants;
    }

    public Boolean getHasCuratorGrants() {
        return hasCuratorGrants;
    }

    public Boolean getHasHRGrants() {
        return hasHRGrants;
    }

    public Boolean getHasAdminGrants() {
        return hasAdminGrants;
    }

    public void setHasCuratorGrants(Boolean hasCuratorGrants) {
        this.hasCuratorGrants = hasCuratorGrants;
    }

    public void setHasHRGrants(Boolean hasHRGrants) {
        this.hasHRGrants = hasHRGrants;
    }

    public void setHasAdminGrants(Boolean hasAdminGrants) {
        this.hasAdminGrants = hasAdminGrants;
    }
}
