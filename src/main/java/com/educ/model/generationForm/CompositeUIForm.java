package com.educ.model.generationForm;

/**
 * Created by anbu0216 on 02.12.2016.
 */
public class CompositeUIForm {
    private UIForm studentForm;
    private UIForm curatorForm;
    private UIForm hrForm;
    private UIForm managerForm;

    public UIForm getStudentForm() {
        return studentForm;
    }

    public void setStudentForm(UIForm studentForm) {
        this.studentForm = studentForm;
    }

    public UIForm getCuratorForm() {
        return curatorForm;
    }

    public void setCuratorForm(UIForm curatorForm) {
        this.curatorForm = curatorForm;
    }

    public UIForm getHrForm() {
        return hrForm;
    }

    public void setHrForm(UIForm hrForm) {
        this.hrForm = hrForm;
    }

    public UIForm getManagerForm() {
        return managerForm;
    }

    public void setManagerForm(UIForm managerForm) {
        this.managerForm = managerForm;
    }
}
