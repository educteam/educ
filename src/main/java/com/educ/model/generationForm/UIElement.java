package com.educ.model.generationForm;

/**
 * Created by anbu0216 on 15.11.2016.
 */
public class UIElement {
    private long id;
    private String title;
    private long order;

    public long getId() {
        return id;
    }

    public void setId(long field_id) {
        this.id = field_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(long order) {
        this.order = order;
    }
}
