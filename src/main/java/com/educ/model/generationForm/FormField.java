package com.educ.model.generationForm;

import java.util.List;

/**
 * Created by INDIGO-ПС on 06.11.2016.
 */
public class FormField extends UIElement {
    private String field_type;
    private String field_value;
    private Boolean field_required;
    private String field_disabled;
    private String description;
    private List<FormOption> field_options;
    private Boolean field_obsolete;

    public String getField_type() {
        return field_type;
    }

    public void setField_type(String field_type) {
        this.field_type = field_type;
    }

    public String getField_value() {
        return field_value;
    }

    public void setField_value(String field_value) {
        this.field_value = field_value;
    }

    public Boolean getField_required() {
        return field_required;
    }

    public void setField_required(Boolean field_required) {
        this.field_required = field_required;
    }

    public String getField_disabled() {
        return field_disabled;
    }

    public void setField_disabled(String field_disabled) {
        this.field_disabled = field_disabled;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public List<FormOption> getField_options() {
        return field_options;
    }

    public void setField_options(List<FormOption> field_options) {
        this.field_options = field_options;
    }

    public Boolean getField_obsolete() {
        return field_obsolete;
    }

    public void setField_obsolete(Boolean field_obsolete) {
        this.field_obsolete = field_obsolete;
    }
}
