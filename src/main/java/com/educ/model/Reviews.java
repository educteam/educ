package com.educ.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by GriGri on 22.10.2016.
 */

@Entity
public class Reviews implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id",unique=true, nullable = false)
    private Long id;
    private Long reviewer;
    private Long refereed;
    private String comment;
    private int mark;
    private Date date;

    public void setId(Long id) {
        this.id = id;
    }
    public void setReviewer(Long reviewer) {
        this.reviewer = reviewer;
    }
    public void setRefereed(Long refereed) {
        this.refereed = refereed;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public void setMark(int mark) {
        this.mark = mark;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }
    public Long getReviewer() {
        return reviewer;
    }
    public Long getRefereed() {
        return refereed;
    }
    public String getComment() {
        return comment;
    }
    public int getMark() {
        return mark;
    }
    public Date getDate() {
        return date;
    }
}
