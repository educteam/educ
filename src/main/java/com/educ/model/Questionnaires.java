package com.educ.model;

import java.util.ArrayList;

/**
 * Created by anbu0216 on 23.11.2016.
 */
public class Questionnaires {
    Long formId;
    ArrayList<String> titles  = new ArrayList<>();
    ArrayList<String> curatorFormTitles  = new ArrayList<>();
    ArrayList<Questionnaire> answers = new ArrayList<>();

    public Questionnaires(Long formId, ArrayList<String> titles, ArrayList<Questionnaire> answers) {
        this.formId = formId;
        this.titles = titles;
        this.answers = answers;
    }

    public Questionnaires(Long formId, ArrayList<String> titles, ArrayList<String> curatorFormTitles, ArrayList<Questionnaire> answers) {
        this.formId = formId;
        this.titles = titles;
        this.curatorFormTitles = curatorFormTitles;
        this.answers = answers;
    }


    public ArrayList<String> getTitles() {
        return titles;
    }

    public Long getFormId() {
        return formId;
    }

    public ArrayList<String> getCuratorFormTitles() {
        return curatorFormTitles;
    }

    public ArrayList<Questionnaire> getAnswers() {
        return answers;
    }
}
