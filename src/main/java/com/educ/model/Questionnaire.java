package com.educ.model;

import java.util.ArrayList;

/**
 * Created by anbu0216 on 23.11.2016.
 */
public class Questionnaire {
    Long id;
    ArrayList<String> studentAnswers = new ArrayList<>();
    ArrayList<String> curatorAnswers = new ArrayList<>();
    public Questionnaire(Long id, ArrayList<String> studentAnswers, ArrayList<String> curatorAnswers) {
        this.id = id;
        this.studentAnswers = studentAnswers;
        this.curatorAnswers = curatorAnswers;
    }

    public Questionnaire(Long id, ArrayList<String> studentAnswers) {
        this.id = id;
        this.studentAnswers = studentAnswers;
    }


    public ArrayList<String> getStudentAnswers() {
        return studentAnswers;
    }

    public ArrayList<String> getCuratorAnswers() {
        return curatorAnswers;
    }

    public Long getId() {
        return id;
    }



}
