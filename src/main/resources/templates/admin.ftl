<!DOCTYPE html>
<html lang="en" ng-app="educ" ng-controller="MainController">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
    <meta http-equiv="CACHE-CONTROL" content="NO-STORE">
    <title ng-bind="Page.title()"></title>
    <link rel="stylesheet" href="/css/bootstrap.css"/>
    <link rel="stylesheet" href="/css/form.css"/>
    <link rel="stylesheet" href="/css/admin.css"/>
</head>
<body>

<div class="netcracker-bar">
    <div class="container">
        <div class="subheader">
            <div class="pull-left" style="display:inline-block">
                <img src="/image/logo.png"/>
            </div>
            <div style="display:inline-block">
                <div class="item"><a href="#/builder/list">Формы</a></div>
            </div>
            <div style="display:inline-block">
                <div class="item"><a href="/logout">Logout</a></div>
            </div>
        </div>
    </div>
</div>

<div class="nectracker-background">

</div>

<div class="container" style="margin-top: -130px">
    <div ng-view></div>
</div>

<script type="text/javascript" src="/js/libs/angular.js"></script>
<script type="text/javascript" src="/js/libs/angular-route.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-cookies.js"></script>
<script type="text/javascript" src="/js/libs/angular-animate.js"></script>
<script type="text/javascript" src="/js/libs/ui-bootstrap-tpls-2.2.0.js"></script>
<script type="text/javascript" src="/js/admin_main.js"></script>
<script type="text/javascript" src="/js/services/form-service.js"></script>
<script type="text/javascript" src="/js/controllers/CreateController.js"></script>
<script type="text/javascript" src="/js/controllers/EditController.js"></script>
<script type="text/javascript" src="/js/controllers/formController.js"></script>
<script type="text/javascript" src="/js/controllers/ListQuestionnairesController.js"></script>
<script type="text/javascript" src="/js/controllers/ListFormsController.js"></script>
<script type="text/javascript" src="/js/controllers/ModalController.js"></script>
<script type="text/javascript" src="/js/directives/form-directive.js"></script>
<script type="text/javascript" src="/js/directives/error-directive.js"></script>
<script type="text/javascript" src="/js/directives/field-directive.js"></script>
<script type="text/javascript" src="/js/controllers/ConfirmDeleteController.js"></script>
</body>
</html>