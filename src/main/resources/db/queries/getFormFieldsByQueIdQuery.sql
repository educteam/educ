/*
* INPUT PARAMETERS:
* 1 - ORDER_ATTRIBUTE
* 2 - OBJECT_TYPE_QUESTIONNAIRE
* 3 - questionnairesId
*/

 SELECT el.object_id as field_id, el.name as field_name FROM objects el
	left join params p ON p.attr_id = ? AND 
	el.object_id = p.object_id 
	join objects q ON el.parent_id = q.parent_id AND 
	q.object_type_id = ? AND 
	q.object_id = ? 
	WHERE el.object_type_id BETWEEN 100 AND 200
	ORDER BY CAST(p.value AS int) ASC