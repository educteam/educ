/* 
* INPUT PARAMETERS:
* 1 - elementOT()
* 2 - elementParentId()
* 3 - elementObjectId()
* 4 - elementName()
* 5 - elementDescription()
*/
 INSERT INTO objects
		(object_id, object_type_id, parent_id, name, description) VALUES(?,?,?,?,?);
