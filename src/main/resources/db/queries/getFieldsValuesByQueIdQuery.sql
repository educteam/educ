/*
* INPUT PARAMETERS:
* 1 - questionnaireId
* 2 - OBJECT_TYPE_QUESTIONNAIRE
*/
 SELECT -1 as stud_questionnaire,
	param.object_id AS questionnaire_id, 
	param.attr_id as field_id,  
	attrs.name as field_name, 
	param.value AS field_value 
 FROM params param
	LEFT JOIN objects attrs ON (attrs.object_id = param.attr_id)  
 WHERE param.object_id = ? AND
	param.attr_id in (  
		 SELECT el.object_id as field_id 
		 FROM objects el  
			JOIN objects q ON el.parent_id = q.parent_id AND  
				q.object_type_id = ? AND  
				q.object_id =  param.object_id 
		 WHERE el.object_type_id between 100 and 200
	 )   
 ORDER BY questionnaire_id,field_id