/*
* INPUT PARAMETERS:
* 1 - ORDER_ATTRIBUTE
* 2 - formId
*/
 SELECT o.object_id AS field_id,
		o.name AS field_name 
 FROM objects o
	LEFT JOIN params p ON p.attr_id = ? AND  
		o.object_id = p.object_id  
 WHERE o.object_type_id BETWEEN 100 AND 200
	 AND o.parent_id = ?
 ORDER BY CAST(p.value AS int) ASC