/*
* INPUT PARAMETERS:
* 1 - OBJECT_TYPE_QUESTIONNAIRE
* 2 - OBJECT_TYPE_FORM
*/		
 SELECT form_obj.object_id AS form_id,
    form_obj.name AS form_name,
    COUNT(quest_obj.*) AS quests_count
 FROM objects form_obj
    LEFT JOIN objects quest_obj ON quest_obj.parent_id = form_obj.object_id AND
								quest_obj.object_type_id = ?
 WHERE form_obj.object_type_id = ?
 GROUP BY form_obj.object_id
