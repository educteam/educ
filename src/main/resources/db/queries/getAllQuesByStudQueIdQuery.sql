/*
* INPUT PARAMETERS:
* 1 - questionnairesId
* 2 - EMPLOYEE_QUESTIONNAIRE_OTs
* 3 - questionnairesId
* 4 - EMPLOYEE_FORM_OTs
* 5 - STUDENT_QUESTIONNAIRE_OT
* 6 - questionnairesId
*/
 (
 SELECT
   obj.object_id as field_id, answers.value as field_value
 FROM objects obj
	JOIN objects que
      ON que.parent_id = ?
	    AND que.object_type_id = ANY(?)/*...IN (Array)*/
	JOIN params answers
      ON answers.object_id = que.object_id
      AND answers.attr_id = obj.object_id
 WHERE
   obj.object_id IN
   (
      SELECT t.object_id
      FROM
         transition t,
         objects q,
         objects f
      WHERE
         q.object_id = ?
         AND q.parent_id = f.parent_id
         AND f.object_type_id = ANY(?)/*...IN (Array)*/
         AND t.parent_id = f.object_id
       	 AND t.level = 1
   )
 )
 UNION
 (
 SELECT el.object_id as field_id, answ.value as field_value
 FROM objects el
	JOIN objects que
    	ON el.parent_id = que.parent_id
        AND que.object_type_id = ?
        AND que.object_id = ?
    JOIN params answ
    	ON answ.object_id = que.object_id
      	AND answ.attr_id = el.object_id
 WHERE el.object_type_id BETWEEN 100 AND 200
 )