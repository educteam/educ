/*
* INPUT PARAMETERS:
* 1 - Object values for update
*/
 UPDATE objects AS el SET
	name = t.name,
	description = t.description
 FROM (values
		%s
	) AS t(object_id, name, description)
 WHERE t.object_id = el.object_id