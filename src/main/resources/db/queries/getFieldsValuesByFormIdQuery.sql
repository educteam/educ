/*
* INPUT PARAMETERS:
* 1 - formId
* ----
* 1th - filterArgs
*/
 SELECT -1 AS stud_questionnaire,
		param.object_id AS questionnaire_id, 
		param.attr_id as field_id,
		attrs.name as field_name, 
		param.value AS field_value 
 FROM params param
    LEFT JOIN objects attrs ON (attrs.object_id = param.attr_id)
    %s
 WHERE param.attr_id in (
			 SELECT object_id 
			 FROM objects el
			 WHERE el.object_type_id BETWEEN 100 AND 200
				AND el.parent_id = ?
		)  
 ORDER BY questionnaire_id, field_id