var app = angular.module('educ', ["ui.bootstrap","ngAnimate","ngRoute","ngCookies"])
    .config(function ($routeProvider) {
        $routeProvider.when('/form/:id/',
            {
                templateUrl: '/views/templates/form.html',
                controller: 'FormController'
            }).when('/',
            {
                redirectTo: '/form/20372/'
            })
            .otherwise({
                templateUrl: '/views/templates/404.html'
            });
    }).factory('Page', function($location){
        var title = 'EDUC';
        var address = "http://"+$location.host()+":"+$location.port();
        return {
            title: function() { return title; },
            address: function() { return address; },
            setTitle: function(newTitle) { title = newTitle; }
        };
    }).controller('MainController',
        function MainController($scope, Page) {
            $scope.Page = Page;
    });


