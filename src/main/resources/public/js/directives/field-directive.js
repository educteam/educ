app.directive('fieldDirective', function($http, $compile) {

    var getTemplateUrl = function(field) {
        var type = field.field_type;
        var templateUrl = './views/directives/field/';
        var supported_fields = [
            'text',
            'textarea',
            'checkbox',
            'date',
            'list',
            'multiplelist',
            'radio',
            'phone_number',
            'email',
            'bounded_number',
            'year_of_birth'
        ];

        if (__indexOf.call(supported_fields, type) >= 0) {
            return templateUrl += type + '.html';
        }
    };

    var linker = function(scope, element) {
        // GET template content from path
        var templateUrl = getTemplateUrl(scope.field);
        $http.get(templateUrl).success(function(data) {
            element.html(data);
            $compile(element.contents())(scope);
        });
    }

    return {
        controller: function($scope){
            $scope.stateChangedSingle= function(field){
                console.log("s");
                console.log(field.field_value);
                for(var i = 0; i < field.field_options.length; i++){
                    field.field_options[i].option_selected = false;
                    console.log(field.field_options[i].option_value);
                    if(field.field_options[i].option_value == field.field_value) {
                        console.log(field.field_options[i].option_value);
                        field.field_options[i].option_selected = true;
                    }

                }

            };


            $scope.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date(2020, 1, 1),
                minDate: new Date(1950, 1, 1),
                startingDay: 1
            };
            $scope.format = "dd.MM.yyyy";




            $scope.checkboxCondition = function(field){
                var checked = false;
                for(var i = 0; i < field.field_options.length; i++){
                    if(field.field_options[i].option_selected == true) checked = true;
                }
                return field.field_required && !checked;
            }

            $scope.openDate = function(field){
                field.opened = true;
            }
        },
        template: '<div ng-bind="field"></div>',
        restrict: 'E',
        scope: {
            field: '='
        },
        link: linker
    };
});

var __indexOf = [].indexOf || function(item) {
        for (var i = 0, l = this.length; i < l; i++) {
            if (i in this && this[i] === item) return i;
        }
        return -1;
    };
