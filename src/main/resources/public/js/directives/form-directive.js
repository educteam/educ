app.directive('formDirective', function ($http,Page) {
    return {
        controller: function($scope){
            $scope.submit = function(){
                $http.post(Page.address()+'/api/public/submitQuestionnaire', $scope.form).then(function(response){
                    $scope.submitted = true;
                }, function(response){
                });

            }

            $scope.cancel = function(){
            }
        },
        templateUrl: './views/directives/form/form.html',
        restrict: 'E',
        scope: {
            form:'='
        }
    };
});