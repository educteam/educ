var app = angular.module('educ', ["ui.bootstrap","ngAnimate","ngRoute","ngCookies"])
    .config(function ($routeProvider) {
        $routeProvider.when('/',
            {
                templateUrl: '/views/templates/forms_list.html',
                controller: 'ListFormsController'
            }).when('/builder/create',
            {
                templateUrl: '/views/templates/create.html',
                controller: 'CreateController'
            }).when('/questionnaires/:id/',
            {
                templateUrl: '/views/templates/list.html',
                controller: 'ListQuestionnairesController'
            }).when('/builder/list/',
            {
                templateUrl: '/views/templates/forms_list.html',
                controller: 'ListFormsController'
            }).when('/builder/edit/:id/',
            {
                templateUrl: '/views/templates/edit.html',
                controller: 'EditController'
            }).when('/form/:id/',
            {
                templateUrl: '/views/templates/form.html',
                controller: 'FormController'
            }).when('/error404',
            {
                templateUrl: '/views/templates/404.html'
            })
            .otherwise({
                templateUrl: '/views/templates/404.html'
            });
    }).factory('Page', function($location){
        var title = 'EDUC';
        var address = "http://"+$location.host()+":"+$location.port();
        return {
            title: function() { return title; },
            address: function() { return address; },
            setTitle: function(newTitle) { title = newTitle; }
        };
    }).controller('MainController',
        function MainController($scope, Page) {
            $scope.Page = Page;
        });

