app.controller('ListFormsController',
    function ListFormsController($scope, $http, $timeout, Page) {
        $scope.grants = {};
        $scope.data = {}
        $scope.hasCuratorGrants = {};
        $scope.hasHRGrants = {};
        $scope.hasAdminGrants = {};
        $http.get(Page.address()+'/api/private/getForms/').then(function (response) {
            $scope.data = response.data;
            //console.log(response.data);
        },function (response){
            console.log(response);
            if(response.status == 403) {
                $window.location.reload();
            }
        });

        $http.get(Page.address()+'/api/private/getGrants').success(function (data) {
            $scope.grants = data;
            $scope.hasCuratorGrants = $scope.grants.hasCuratorGrants;
            $scope.hasHRGrants = $scope.grants.hasHRGrants;
            $scope.hasAdminGrants = $scope.grants.hasAdminGrants;
        });
    }
);