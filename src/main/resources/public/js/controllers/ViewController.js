/**
 * Created by GriGri on 20.11.2016.
 */
app.controller('EditController',
    function EditController($scope, $http, $routeParams,FormService,Page) {
        $scope.form = {};
        $scope.addField = {};
        $scope.addField.types = FormService.fields;
        $scope.addField.new = $scope.addField.types[0].name;
        //$scope.addField.name = "New Field";
        $scope.lastindex = 0;
        $http.get(Page.address()+'/api/private/getView?id='+$routeParams.id).success(function (data) {
            $scope.form = data;
            for(var i = 0; i < $scope.form.form_fields.length; i++){
                $scope.form.form_fields[i].field_index = ++$scope.lastindex;
                for(var j = 0; j < $scope.form.form_fields[i].field_options.length; j++){
                    $scope.form.form_fields[i].field_options[j].option_index = j+1;
                }
            }
        });
    });