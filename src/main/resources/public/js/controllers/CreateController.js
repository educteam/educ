app.controller('CreateController',
    function CreateController($scope, $http,$routeParams,FormService,$uibModal,Page) {
        $scope.forms = {};
        $scope.forms.studentForm = {};
        $scope.forms.studentForm.title = 'Form';
        $scope.forms.studentForm.id = 0;
        $scope.forms.studentForm.form_fields = [];
        $scope.forms.studentForm.order = 0;
        $scope.addField = {};
        $scope.addField.types = FormService.fields;
        $scope.addField.new = $scope.addField.types[0].name;
        $scope.addField.name = "New Field";


        $scope.addCField = {};
        $scope.addCField.types = FormService.fields;
        $scope.addCField.new = $scope.addCField.types[0].name;
        $scope.addCField.name = "New Curator Field";

        $scope.addHRField = {};
        $scope.addHRField.types = FormService.fields;
        $scope.addHRField.new = $scope.addHRField.types[0].name;
        $scope.addHRField.name = "New HR Field";

        $scope.addMField = {};
        $scope.addMField.types = FormService.fields;
        $scope.addMField.new = $scope.addMField.types[0].name;
        $scope.addMField.name = "New Manager Field";

        $scope.forms.curatorForm = {};
        $scope.forms.curatorForm.title = 'Curator form';
        $scope.forms.curatorForm.id = 0;
        $scope.forms.curatorForm.form_fields = [];
        $scope.forms.curatorForm.order = 0;

        $scope.forms.hrForm = {};
        $scope.forms.hrForm.title = 'HR form';
        $scope.forms.hrForm.id = 0;
        $scope.forms.hrForm.form_fields = [];
        $scope.forms.hrForm.order = 0;

        $scope.forms.managerForm = {};
        $scope.forms.managerForm.title = 'Manager form';
        $scope.forms.managerForm.id = 0;
        $scope.forms.managerForm.form_fields = [];
        $scope.forms.managerForm.order = 0;

        /*$scope.forms.push($scope.studentForm);
        $scope.forms.push($scope.curatorForm);*/
        $scope.lastindex = 0;


        $scope.getFormID = function(form){
            $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                form.id = data;
            });
        }

        //Закомментить чтобы подлить норм анкету
        $scope.getFormID($scope.forms.studentForm);
        $scope.getFormID($scope.forms.curatorForm);
        $scope.getFormID($scope.forms.hrForm);
        $scope.getFormID($scope.forms.managerForm);

        //Раскомментить чтобы подлить норм анкету
        //$scope.forms = {"studentForm":{"title":"Form","id":41082,"form_fields":[{"id":41084,"order":1,"title":"ФИО","field_type":"text","field_value":"","field_required":"true","field_disabled":false},{"id":41085,"order":2,"title":"E-Mail","field_type":"email","field_value":"","field_required":"true","field_disabled":false},{"id":41086,"order":3,"title":"Номер мобильного телефона (с кодом страны и города)","field_type":"phone_number","field_value":"","field_required":"true","field_disabled":false},{"id":41087,"order":4,"title":"Skype","field_type":"text","field_value":"","field_required":false,"field_disabled":false},{"id":41088,"order":5,"title":"Любые другие контактные данные","field_type":"text","field_value":"","field_required":false,"field_disabled":false},{"id":41089,"order":6,"title":"Год рождения","field_type":"bounded_number","field_value":"","field_required":"true","field_disabled":false},{"id":41090,"order":7,"title":"ВУЗ","field_type":"text","field_value":"","field_required":"true","field_disabled":false},{"id":41091,"order":8,"title":"Курс","field_type":"text","field_value":"","field_required":"true","field_disabled":false},{"id":41092,"order":9,"title":"Факультет","field_type":"text","field_value":"","field_required":"true","field_disabled":false},{"id":41093,"order":10,"title":"Кафедра и направление подготовки","field_type":"text","field_value":"","field_required":"true","field_disabled":false},{"id":41094,"order":11,"title":"Желаемое направление обучения","field_type":"radio","field_value":"","field_required":"true","field_disabled":false,"field_options":[{"order":1,"id":41095,"title":"Software Development","option_value":1},{"order":2,"id":41096,"title":"Business Analysis","option_value":2}]},{"id":41097,"order":12,"title":"Родной город. Для иностранных граждан укажите также гражданство.","field_type":"text","field_value":"","field_required":"true","field_disabled":false},{"id":41098,"order":13,"title":"Сколько часов в неделю вы можете работать?","field_type":"bounded_number","field_value":"","field_required":"true","field_disabled":false},{"id":41099,"order":14,"title":"Уровень знания английского языка","field_type":"radio","field_value":"","field_required":"true","field_disabled":false,"field_options":[{"order":1,"id":41100,"title":"Свободно читаю, пишу и говорю","option_value":1},{"order":2,"id":41101,"title":"Свободно читаю и пишу, мало разговорного опыта","option_value":2},{"order":3,"id":41102,"title":"Свободно читаю, мало письменного и разговорного опыта","option_value":3},{"order":4,"id":41103,"title":"Читаю со словарём, мало письменного и разговорного опыта","option_value":4}]},{"id":41104,"order":15,"title":"Уровень знания Java","field_type":"radio","field_value":"","field_required":"true","field_disabled":false,"field_options":[{"order":1,"id":41105,"title":"0 (ничего не знаю и нет никакого опыта)","option_value":1},{"order":2,"id":41106,"title":"1 (начальные знания, совсем нет опыта)","option_value":2},{"order":3,"id":41107,"title":"2 (начальные знания, минимальный опыт)","option_value":3},{"order":4,"id":41108,"title":"3 (имею 1+ лет опыт работы в коммерческих компаниях)","option_value":4},{"order":5,"id":41109,"title":"4 (имею 2+ лет опыт работы в коммерческих компаниях)","option_value":5},{"order":6,"id":41110,"title":"5 (знаю тонкости языка, имею 3+ лет опыта работы в коммерческих компаниях)","option_value":6}]},{"id":41111,"order":16,"title":"Уровень знания SQL","field_type":"radio","field_value":"","field_required":"true","field_disabled":false,"field_options":[{"order":1,"id":41112,"title":"0 (ничего не знаю или нет никакого опыта)","option_value":1},{"order":2,"id":41113,"title":"1 (начальные знания, совсем нет опыта)","option_value":2},{"order":3,"id":41114,"title":"2 (начальные знания, минимальный опыт)","option_value":3},{"order":4,"id":41115,"title":"3 (имею 1+ лет опыт работы в коммерческих компаниях)","option_value":4},{"order":5,"id":41116,"title":"4 (имею 2+ лет опыт работы в коммерческих компаниях)","option_value":5},{"order":6,"id":41117,"title":"5 (знаю тонкости языка, имею 3+ лет опыта работы в коммерческих компаниях)","option_value":6}]},{"id":41118,"order":17,"title":"Какие языки программирования знаете/использовали","field_type":"text","field_value":"","field_required":"false","field_disabled":false,"description":"C#, C/C++, Perl, PHP, JavaScript и т.п."},{"id":41119,"order":18,"title":"Что заинтересовало в обучении у нас:","field_type":"radio","field_value":"","field_required":"true","field_disabled":false,"field_options":[{"order":1,"id":41120,"title":"Опыт решения практических задач","option_value":1},{"order":2,"id":41121,"title":"Трудоустройство","option_value":2},{"order":3,"id":41122,"title":"Обучение","option_value":3}]},{"id":41123,"order":19,"title":"Как узнали о наборе на обучение?","field_type":"radio","field_value":"","field_required":"true","field_disabled":false,"field_options":[{"order":1,"id":41124,"title":"сотрудники ВУЗа","option_value":1},{"order":2,"id":41125,"title":"Печатное объявление в ВУЗе","option_value":2},{"order":3,"id":41126,"title":"Друзья","option_value":3},{"order":4,"id":41127,"title":"Вконтакте","option_value":4}]},{"id":41128,"order":20,"title":"Когда вы можете пройти СОБЕСЕДОВАНИЕ в офисе Netcracker Technology?","field_type":"date","field_value":"","field_required":"true","field_disabled":false},{"id":41129,"order":21,"title":"Любая дополнительная информация, которую бы вы хотели нам сообщить","field_type":"textarea","field_value":"","field_required":false,"field_disabled":false},{"id":41130,"order":22,"title":"СОГЛАСИЕ НА ОБРАБОТКУ ПЕРСОНАЛЬНЫХ ДАННЫХ","field_type":"radio","field_value":"","field_required":"true","field_disabled":false,"description":"В соответствие с требованиями действующего законодательства РФ, просим дать Обществу с ограниченной ответственностью \"НетКрэкер\" (ИНН 7713511177; российское отделение международной компании Netcracker Technology) СОГЛАСИЕ НА ОБРАБОТКУ ПЕРСОНАЛЬНЫХ ДАННЫХ, указанных в данной анкете (включая сбор, систематизацию, накопление, уточнение, использование, обезличивание, уничтожение), в течение 5 лет с момента получения - в целях организации обучения и/или рассмотрения в качестве кандидата на работу в ООО \"НетКрэкер\". Даю согласие на обработку персональных данных, указанных в данной анкете.","field_options":[{"order":1,"id":41131,"title":"да, согласен","option_value":1},{"order":2,"id":41132,"title":"нет, не согласен на обработку персональных данных","option_value":2}]}],"order":0},"hrForm":{"title":"HR form","id":41083,"form_fields":[{"id":41133,"order":23,"title":"HR","field_type":"text","field_value":"","field_required":false,"field_disabled":false},{"id":41134,"order":24,"title":"HR Брать?","field_type":"list","field_value":"","field_required":false,"field_disabled":false,"field_options":[{"order":1,"id":41135,"title":"Брать ко мне","option_value":1},{"order":2,"id":41136,"title":"Брать если только... (указать подробности в отзыве)","option_value":2},{"order":3,"id":41137,"title":"Не брать","option_value":3}]},{"id":41138,"order":25,"title":"Отзыв HR","field_type":"textarea","field_value":"","field_required":false,"field_disabled":false}],"order":0},"curatorForm":{"title":"Curator form","id":42000,"form_fields":[{"id":41139,"order":26,"title":"Куратор","field_type":"text","field_value":"","field_required":false,"field_disabled":false},{"id":41140,"order":27,"title":"Куратор Брать?","field_type":"list","field_value":"","field_required":false,"field_disabled":false,"field_options":[{"order":1,"id":41141,"title":"Брать ко мне","option_value":1},{"order":2,"id":41142,"title":"Брать если только... (указать подробности в отзыве)","option_value":2},{"order":3,"id":41143,"title":"Не брать","option_value":3}]},{"id":41144,"order":28,"title":"Отзыв куратора","field_type":"textarea","field_value":"","field_required":false,"field_disabled":false}],"order":0},"managerForm":{"title":"Manager form","id":42001,"form_fields":[{"id":41145,"order":29,"title":"Менеджер","field_type":"text","field_value":"","field_required":false,"field_disabled":false},{"id":41146,"order":30,"title":"Менеджер Брать?","field_type":"list","field_value":"","field_required":false,"field_disabled":false,"field_options":[{"order":1,"id":41147,"title":"Брать ко мне","option_value":1},{"order":2,"id":41148,"title":"Брать если только... (указать подробности в отзыве)","option_value":2},{"order":3,"id":41149,"title":"Не брать","option_value":3}]},{"id":41150,"order":31,"title":"Отзыв Менеджера","field_type":"textarea","field_value":"","field_required":false,"field_disabled":false}],"order":0}};
        $scope.addNewField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addField.name,
                "field_type" : $scope.addField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        };

        $scope.addNewCField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addCField.name,
                "field_type" : $scope.addCField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        }

        $scope.addNewHRField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addHRField.name,
                "field_type" : $scope.addHRField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        }

        $scope.addNewMField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addMField.name,
                "field_type" : $scope.addMField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        }

        $scope.getFieldID = function(form,field_id){
                $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                    for(var i = 0; i < form.form_fields.length; i++){
                        if(form.form_fields[i].order == field_id){
                            form.form_fields[i].id = data;
                            break;
                        }
                    }
                });
        }

        $scope.getOptionID = function(field,option){
            $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                for(var i = 0; i < field.field_options.length; i++){
                    if(field.field_options[i].order == option.order){
                        field.field_options[i].id = data;
                        break;
                    }
                }
            });
        }



        $scope.deleteField = function (form,field_id){
            for(var i = 0; i < form.form_fields.length; i++){
                if(form.form_fields[i].id == field_id){
                    form.form_fields.splice(i, 1);
                    break;
                }
            }
        }

        $scope.showAddOptions = function (field){
            if(field.field_type == "radio" || field.field_type == "list" || field.field_type == "checkbox")
                return true;
            else
                return false;
        }

        $scope.addOption = function (field){
            if(!field.field_options)
                field.field_options = new Array();

            var lastOptionID = 0;

            if(field.field_options[field.field_options.length-1])
                lastOptionID = field.field_options[field.field_options.length-1].order;

            // new option's id
            var option_id = lastOptionID + 1;

            var newOption = {
                "order" : option_id,
                "id" : 0,
                "title" : "Option " + option_id,
                "option_value" : option_id
            };

            // put new option into field_options array
            field.field_options.push(newOption);
            $scope.getOptionID(field,newOption);
        }

        $scope.deleteOption = function (field, option){
            for(var i = 0; i < field.field_options.length; i++){
                if(field.field_options[i].order == option.order){
                    field.field_options.splice(i, 1);
                    break;
                }
            }
        }

        $scope.save = function(){
            if($scope.forms.studentForm.form_fields.length == 0) {
                alert("Create Student Form!");
                return;
            }
            if($scope.forms.curatorForm.form_fields.length == 0) {
                alert("Create Curator Form!");
                return;
            }
            if($scope.forms.hrForm.form_fields.length == 0) {
                alert("Create HR Form!");
                return;
            }
            $http.post(Page.address()+'/api/private/createForm', $scope.forms).then(function(response){
                $uibModal.open({
                    templateUrl: "./views/templates/modal/create-form-modal.html",
                    controller: "CreateModalController",
                    backdrop  : 'static',
                    keyboard  : false
                });
                console.log("Sucsess " + response);
            }, function(response){
                console.log("Fail "+ response);
            });
        }
    });

app.controller('CreateModalController',
    function CreateModalController($scope,$uibModalInstance,$location) {
        $scope.submit = function(){
            $uibModalInstance.close();
            $location.path("/builder/list/");
        }

    });