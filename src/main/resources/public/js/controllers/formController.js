app.controller('FormController',
    function FormController($scope, $http,$routeParams,Page) {
        Page.setTitle("Student Form");
        console.log(Page.title());
        //$scope.id = $routeParams.id;
        $scope.form = {}
        $scope.loaded = false;
        $http.get(Page.address()+'/api/public/getStudentFormForSurvey/?id='+$routeParams.id).success(function (data) {
            $scope.form = data;
            Page.setTitle(data.title);
            $scope.loaded = true;
        });
    }
);