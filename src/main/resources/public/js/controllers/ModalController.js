app.controller('ModalController',
    function ModalController($scope, $http, $routeParams, $uibModalInstance, $location, Page) {
        $scope.data = {};
        $scope.hasCuratorGrants = {};
        $scope.hasHRGrants = {};
        $scope.hasAdminGrants = {};
        $http.get(Page.address()+'/api/private/getGrants').success(function (data) {
            $scope.grants = data;
            $scope.hasCuratorGrants = $scope.grants.hasCuratorGrants;
            $scope.hasHRGrants = $scope.grants.hasHRGrants;
            $scope.hasAdminGrants = $scope.grants.hasAdminGrants;
        });

        $http.get(Page.address()+'/api/private/getAnswers/?id='+$routeParams.idQuestionnair).success(function (data) {
            $scope.data = data;
        });
        $scope.curatorForm = {};
        $http.get(Page.address()+'/api/private/getCuratorForm/?id='+$routeParams.idQuestionnair ).success(function (data) {
            if ($scope.hasHRGrants && !$scope.hasAdminGrants){
                for (var i = 0; i < data.form_fields.length; i++){
                    data.form_fields[i].field_required = false;
                    data.form_fields[i].field_disabled = true;
                }
            }
            $scope.curatorForm = data;
        });

        $scope.hrForm = {};
        $http.get(Page.address()+'/api/private/getHRForm/?id='+$routeParams.idQuestionnair ).success(function (data) {
            $scope.hrForm = data;
        });

        $scope.submit = function() {
            if (!$scope.hasHRGrants || $scope.hasAdminGrants){
                $scope.curatorForm.objectTypeName = 'CURATOR_FORM';
                $http.post(Page.address() + '/api/public/submitQuestionnaire', $scope.curatorForm).then(function (response) {
                    $uibModalInstance.close();
                    $location.path("/questionnaires/" + $routeParams.id);
                }, function (response) {
                });
            }
            $scope.hrForm.objectTypeName = 'HR_FORM';
            $http.post(Page.address()+'/api/public/submitQuestionnaire', $scope.hrForm).then(function(response){
                $uibModalInstance.close();
                $location.path("/questionnaires/"+ $routeParams.id);
            }, function(response){
            });
        };

        $scope.isValid = function(){
            return ($scope.hrForm.$valid)
        }

    }
);