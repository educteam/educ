app.controller('ListQuestionnairesController',
    function ListQuestionnairesController($scope, $http,$routeParams, $uibModal, $cookies,$filter,Page) {
        $scope.data = {}

        $scope.titles = [];
        $scope.currentPage = 1;
        $scope.itemsPerPage = 5;
        $scope.totalItems = 0;
        $scope.answers = [];
        $scope.hasCuratorGrants = {};
        $scope.hasHRGrants = {};
        $scope.hasAdminGrants = {};
        $http.get(Page.address()+'/api/private/getGrants').success(function (data) {
            $scope.grants = data;
            $scope.hasCuratorGrants = $scope.grants.hasCuratorGrants;
            $scope.hasHRGrants = $scope.grants.hasHRGrants;
            $scope.hasAdminGrants = $scope.grants.hasAdminGrants;
        });
        $scope.formId = $routeParams.id;
            var loaded = false;
            $http.get(Page.address()+'/api/private/getQuestionnaires/?id='+$routeParams.id).success(function (data) {
                $scope.data = data;
                $scope.titles = $scope.data.titles.concat($scope.data.curatorTitles).concat($scope.data.hrTitles);

                $scope.configurateTitles($scope.data.titles);
                $scope.configurateTitles($scope.data.curatorTitles);
                $scope.configurateTitles($scope.data.hrTitles);
                $scope.totalItems = $scope.data.answers.length;
                $scope.rebuild();
                $scope.filtereded = $scope.data.answers;
                pageInit($scope.currentPage);
                loaded = true;
            });

        $scope.configurateTitles = function(titles) {
            for(var i = 0;i < $scope.titles.length;i++){
                for(var j = 0; j < titles.length; j++){
                    if($scope.titles[i].field_id == titles[j].field_id){
                        var field_id = $scope.titles[i].field_id;
                        var field = $cookies.get("field_"+field_id);
                        if(field === undefined && i < 7) field = true;
                        var result = (field == "true" || field == true);
                        $scope.titles[i].visible = result;
                        $scope.titles[i].checked = result;
                    }
                }
            }
        };

        $scope.$watch("currentPage", function() {
            if(loaded) {
                pageInit($scope.currentPage);
            }
        });
        $scope.$watch('search', function(term) {
            if(loaded) {
                $scope.filtereded = $filter('filter')($scope.data.answers, term);
                $scope.totalItems = $scope.filtereded.length;
                pageInit(1);
            }
        });

        function pageInit(page) {
            var pagedData = $scope.filtereded.slice(
                (page - 1) * $scope.itemsPerPage,
                page * $scope.itemsPerPage
            );
            $scope.answers = pagedData;
        }

        $scope.rebuild = function(){
            for(var i = 0;i < $scope.titles.length;i++){
                $scope.titles[i].visible = $scope.titles[i].checked;
                $cookies.put("field_"+$scope.titles[i].field_id,$scope.titles[i].checked);
                //console.log("field_"+$scope.titles[i].field_id+" "+$cookies.get("field_"+$scope.titles[i].field_id))
            }
            for(var i = 0;i < $scope.data.answers.length;i++){
                var j = 0,k=0,l=0,m=0;
                for(j = 0;j < $scope.data.answers[i].studentAnswers.length;j++){
                    $scope.data.answers[i].studentAnswers[j].visible = $scope.titles[j].visible;
                }
                for(k = j;k < $scope.data.answers[i].curatorAnswers.length+j;k++){
                    $scope.data.answers[i].curatorAnswers[k-j].visible = $scope.titles[k].visible;
                }
                for(l = k;l < $scope.data.answers[i].hrAnswers.length+k;l++){
                    $scope.data.answers[i].hrAnswers[l-k].visible = $scope.titles[l].visible;
                }
            }
        }

/*        $scope.$on("callApplyFilter", function(){
            $scope.applyFilter();
        });*/

        $scope.showComplex = function(id) {
            $routeParams.idQuestionnair = id;
            $uibModal.open({
                templateUrl: "./views/templates/modal.html",
                controller: "ModalController"
            });
        };

        $scope.applyFilter = function(data) {
            $scope.filteredDataRequest = getFilteredData();
            console.log($scope.filteredDataRequest);
            $http.post(Page.address()+'/api/private/getFilteredQuestionnaire', $scope.filteredDataRequest).then(function(response){
                $scope.data.answers = response.data.answers;
                $scope.totalItems = $scope.data.answers.length;
                $scope.rebuild();
                $scope.filtereded = $scope.data.answers;
                pageInit(1);
            }, function(response){
                console.log("Fail "+ response);
            });
        };

        $scope.exportFilteredTable = function(data) {
            $scope.filteredDataRequest = getFilteredData();
            $scope.filteredDataRequest.shownFields = [];
            for (var i = 0; i < $scope.filtereded[0].studentAnswers.length; i++){
                var el = $scope.filtereded[0].studentAnswers[i];
                if (el.visible){
                    $scope.filteredDataRequest.shownFields.push(el.field_id);
                }
            }
            for (var i = 0; i < $scope.filtereded[0].curatorAnswers.length; i++){
                var el = $scope.filtereded[0].curatorAnswers[i];
                if (el.visible){
                    $scope.filteredDataRequest.shownFields.push(el.field_id);
                }
            }
            for (var i = 0; i < $scope.filtereded[0].hrAnswers.length; i++){
                var el = $scope.filtereded[0].hrAnswers[i];
                if (el.visible){
                    $scope.filteredDataRequest.shownFields.push(el.field_id);
                }
            }
            $http.post(Page.address()+'/api/private/exportFilteredTable', $scope.filteredDataRequest, {responseType: "arraybuffer"}).then(function(data){
                var fileName = getFileNameFromHeader(data.headers('content-disposition'));
                var blob = new Blob([data.data], {type: "application/ms-excel"});
                var objectUrl = URL.createObjectURL(blob);
                var anchor = document.createElement("a");
                anchor.download = fileName;
                anchor.href = objectUrl;
                anchor.click();
            }, function(response){
                console.log("Fail "+ response);
            });
        };

        function getFileNameFromHeader(header){
            if (!header) return null;
            var result = header.split(";")[1].trim().split("=")[1];
            return result.replace(/"/g, '');
        }

        function getFilteredData(elements) {
            var applyFilterBtn = document.getElementsByClassName('applyFilterBtn')[0];
            var studentFilters = {};
            var curatorFilters = {};
            var hrFilters = {};
            if (applyFilterBtn.style.display != "none") {
                studentFilters = document.getElementsByClassName('studentFilterFieldClass');
                curatorFilters = document.getElementsByClassName('curatorFilterFieldClass');
                hrFilters = document.getElementsByClassName('hrFilterFieldClass');
            }
            $scope.filteredDataRequest = {};
            $scope.filteredDataRequest.formId = $scope.formId;
            $scope.filteredDataRequest.studentFilters = [];
            for(var i = 0; i < studentFilters.length; i++){
                var el = {};
                el.id=studentFilters[i].name;
                el.value=studentFilters[i].value;
                $scope.filteredDataRequest.studentFilters.push(el);
            }
            $scope.filteredDataRequest.curatorFilters = [];
            for(var i = 0; i < curatorFilters.length; i++){
                var el = {};
                el.id=curatorFilters[i].name;
                el.value=curatorFilters[i].value;
                $scope.filteredDataRequest.curatorFilters.push(el);
            }
            $scope.filteredDataRequest.hrFilters = [];
            for(var i = 0; i < hrFilters.length; i++){
                var el = {};
                el.id=hrFilters[i].name;
                el.value=hrFilters[i].value;
                $scope.filteredDataRequest.hrFilters.push(el);
            }
            return $scope.filteredDataRequest;
        }

        $scope.openSettings = function () {
            var temp_titles = $scope.titles;
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/templates/settings-modal.html',
                controller: 'ModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return temp_titles;
                    }
                }
            });
            modalInstance.result.then(function (items) {
               $scope.titles = items;
               $scope.rebuild();
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

   //     $http.get(Page.address()+'/api/private/getQuestionnaires/?id='+$routeParams.id).success(function (data) {
        $scope.exportQue = function(exportQueId) {
            $http.get(Page.address()+'/api/private/exportQueAsDocx/?queId='+exportQueId, {responseType: "arraybuffer"}).then(function(data){
                var fileName = getFileNameFromHeader(data.headers('content-disposition'));
                var blob = new Blob([data.data], {type: "application/msword"});
                var objectUrl = URL.createObjectURL(blob);
                var anchor = document.createElement("a");
                anchor.download = fileName;
                anchor.href = objectUrl;
                anchor.click();
            }, function(response){
                console.log("Fail "+ response);
            });
        };

        $scope.showConfirmDeletePopup = function(deleteQueId) {
            $routeParams.deleteQueId = deleteQueId;
            $uibModal.open({
                templateUrl: "./views/templates/confirmDeletePage.html",
                controller: "ConfirmDeleteController"
            });
        };

        $scope.hideShowFilters = function () {
            var applyFilterBtn = document.getElementsByClassName('applyFilterBtn')[0];
            var studentFilters = document.getElementsByClassName('studentFilterFieldClass');
            var curatorFilters = document.getElementsByClassName('curatorFilterFieldClass');
            var hrFilters = document.getElementsByClassName('hrFilterFieldClass');
            if (applyFilterBtn.style.display == "none") {
                document.getElementById('showHideBtn').className = "glyphicon glyphicon-menu-up";
                showAllElementsInCollection(studentFilters);
                showAllElementsInCollection(curatorFilters);
                showAllElementsInCollection(hrFilters);
                applyFilterBtn.style.display = "block";
            }else{
                document.getElementById('showHideBtn').className = 'glyphicon glyphicon-menu-down';
                hideAllElementsInCollection(studentFilters);
                hideAllElementsInCollection(curatorFilters);
                hideAllElementsInCollection(hrFilters);
                applyFilterBtn.style.display = "none";
               // $scope.applyFilter();
            }
        };

        function hideAllElementsInCollection(elements) {
            for(var i = 0; i < elements.length; i++){
                var el = elements[i];
                el.style.display = 'none';
            }
        }
        function showAllElementsInCollection(elements) {
            for(var i = 0; i < elements.length; i++){
                var el = elements[i];
                el.style.display = 'block';
            }
        }

        $scope.exportCompositeTable = function () {
            $http.get(Page.address()+'/api/private/exportCompositeTable/?formId=' + $routeParams.id).success(function () {
                console.log("Composite table was exported");
                alert("Form is exported");
            });
        };

        $scope.show = function(element){
            return element.visible == true;
        }


    }
);
app.controller('ModalInstanceCtrl',
    function ModalInstanceCtrl($scope,$uibModalInstance, items) {
        $scope.save = function(){
            $uibModalInstance.close(items);
        }

    });
