app.controller('EditController',
    function EditController($scope, $http,$routeParams,FormService,$uibModal,Page) {
        $scope.forms = {};
        $scope.addField = {};
        $scope.addField.types = FormService.fields;
        $scope.addField.new = $scope.addField.types[0].name;
        $scope.addField.name = "New Field";

        $scope.addCField = {};
        $scope.addCField.types = FormService.fields;
        $scope.addCField.new = $scope.addCField.types[0].name;
        $scope.addCField.name = "New Curator Field";

        $scope.addHRField = {};
        $scope.addHRField.types = FormService.fields;
        $scope.addHRField.new = $scope.addHRField.types[0].name;
        $scope.addHRField.name = "New HR Field";

        $scope.addMField = {};
        $scope.addMField.types = FormService.fields;
        $scope.addMField.new = $scope.addMField.types[0].name;
        $scope.addMField.name = "New Manager Field";

        $scope.lastindex = 0;
        $http.get(Page.address()+'/api/public/getStudentFormForUpdate?id='+$routeParams.id).success(function (data) {
            $scope.forms.studentForm = data;
            for(var i = 0; i < $scope.forms.studentForm.form_fields.length; i++){
                $scope.forms.studentForm.form_fields[i].order = ++$scope.lastindex;
                $scope.forms.studentForm.form_fields[i].field_required =
                    $scope.forms.studentForm.form_fields[i].field_required.toString();
                for(var j = 0; j < $scope.forms.studentForm.form_fields[i].field_options.length; j++){
                    $scope.forms.studentForm.form_fields[i].field_options[j].order = j+1;
                }
            }
        });

        $http.get(Page.address()+'/api/private/getCuratorFormForUpdate?id='+$routeParams.id).success(function (data) {
            $scope.forms.curatorForm = data;
            for(var i = 0; i < $scope.forms.curatorForm.form_fields.length; i++){
                $scope.forms.curatorForm.form_fields[i].order = ++$scope.lastindex;
                $scope.forms.curatorForm.form_fields[i].field_required =
                    $scope.forms.curatorForm.form_fields[i].field_required.toString();
                for(var j = 0; j < $scope.forms.curatorForm.form_fields[i].field_options.length; j++){
                    $scope.forms.curatorForm.form_fields[i].field_options[j].order = j+1;
                }
            }
        });

        $http.get(Page.address()+'/api/private/getHRFormForUpdate?id='+$routeParams.id).success(function (data) {
            $scope.forms.hrForm = data;
            for(var i = 0; i < $scope.forms.hrForm.form_fields.length; i++){
                $scope.forms.hrForm.form_fields[i].order = ++$scope.lastindex;
                $scope.forms.hrForm.form_fields[i].field_required =
                    $scope.forms.hrForm.form_fields[i].field_required.toString();
                for(var j = 0; j < $scope.forms.hrForm.form_fields[i].field_options.length; j++){
                    $scope.forms.hrForm.form_fields[i].field_options[j].order = j+1;
                }
            }
        });




        $scope.addNewField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addField.name,
                "field_type" : $scope.addField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false,
                "field_obsolete" : false,
                "new" : true
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        };

        $scope.addNewCField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addCField.name,
                "field_type" : $scope.addCField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false,
                "field_obsolete" : false,
                "new" : true
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        };

        $scope.addNewHRField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addHRField.name,
                "field_type" : $scope.addHRField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false,
                "new" : true
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        }

        $scope.addNewMField = function(form){
            $scope.lastindex++;
            var newField = {
                "id" : 0,
                "order" : $scope.lastindex,
                "title" : $scope.addMField.name,
                "field_type" : $scope.addMField.new,
                "field_value" : "",
                "field_required" : false,
                "field_disabled" : false,
                "new" : true
            };
            form.form_fields.push(newField);
            $scope.getFieldID(form,newField.order);
        }

        $scope.getFieldID = function(form,field_id){
            $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                for(var i = 0; i < form.form_fields.length; i++){
                    if(form.form_fields[i].order == field_id){
                        form.form_fields[i].id = data;
                        break;
                    }
                }
            });
        }

        $scope.getOptionID = function(field,option){
            $http.get(Page.address()+'/api/private/getObjectId').success(function (data) {
                for(var i = 0; i < field.field_options.length; i++){
                    if(field.field_options[i].order == option.order){
                        field.field_options[i].id = data;
                        break;
                    }
                }
            });
        }

        $scope.deleteField = function (form,field_id){
            for(var i = 0; i < form.form_fields.length; i++){
                if(form.form_fields[i].id == field_id){
                    form.form_fields.splice(i, 1);
                    break;
                }
            }
        }

        $scope.switchField = function (form,field_id){
            for(var i = 0; i < form.form_fields.length; i++){
                if(form.form_fields[i].id == field_id){
                    form.form_fields[i].field_obsolete = !form.form_fields[i].field_obsolete;
                    break;
                }
            }
        }

        $scope.showAddOptions = function (field){
            if(field.field_type == "radio" || field.field_type == "list" || field.field_type == "checkbox")
                return true;
            else
                return false;
        }

        $scope.addOption = function (field){
            if(!field.field_options)
                field.field_options = new Array();

            var lastOptionID = 0;

            if(field.field_options[field.field_options.length-1])
                lastOptionID = field.field_options[field.field_options.length-1].order;

            // new option's id
            var option_id = lastOptionID + 1;

            var newOption = {
                "order" : option_id,
                "id" : 0,
                "title" : "Option " + option_id,
                "option_value" : option_id,
                "option_obsolete" : false,
                "new" : true
            };

            // put new option into field_options array
            field.field_options.push(newOption);
            $scope.getOptionID(field,newOption);
        }

        $scope.deleteOption = function (field, option){
            for(var i = 0; i < field.field_options.length; i++){
                if(field.field_options[i].order == option.order){
                    field.field_options.splice(i, 1);
                    break;
                }
            }
        }

        $scope.switchOption = function (field, option){
            for(var i = 0; i < field.field_options.length; i++){
                if(field.field_options[i].order == option.order){
                    field.field_options[i].option_obsolete = !field.field_options[i].option_obsolete;
                    break;
                }
            }
        }

        $scope.save = function(){
            $http.post(Page.address()+'/api/private/updateForm', $scope.forms).then(function(response){
                $uibModal.open({
                    templateUrl: "./views/templates/modal/create-form-modal.html",
                    controller: "CreateModalController",
                    backdrop  : 'static',
                    keyboard  : false
                });
                console.log("Success " + response);
            }, function(response){
                console.log("Fail "+ response);
            });

            console.log("Form:" + $scope.forms);
        }

    });